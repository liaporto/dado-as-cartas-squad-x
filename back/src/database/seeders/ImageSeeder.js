const faker = require("faker-br");
const Image = require("../../models/Image");

const seedImage = async function() {
    await Image.sync({ force: true });
    
    try{
        for (let i = 0; i < 10; i++) {
            let image = await Image.create({
                id: i,
                path: 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.shoptime.com.br%2Fbusca%2Fgame-board&psig=AOvVaw0ZQ-e6wjw-IadMpnQBttI-&ust=1628297255693000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCPjU5I-Wm_ICFQAAAAAdAAAAABAD',
            });
        }
    } catch(e) {
        console.log(e + '!');
    }
};

module.exports = seedImage;
