require('../../config/dotenv')();
require('../../config/sequelize');

//const seedModel = require('./Model');

const seedUser = require('./UserSeeder');

(async () => {
  try {
    //await seedModel();
    
    await seedUser();

  } catch(err) { console.log(err) }
})();
