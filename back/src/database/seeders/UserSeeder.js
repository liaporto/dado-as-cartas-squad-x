const User = require("../../models/User");
const Image = require("../../models/Image");
const Auth = require("../../config/auth");
const faker = require('faker-br');

const seedUser = async function() {
    try{
        await User.sync({ force: true });
        const users = [];

        const password = 'senha';
        const hashAndSalt = Auth.generatePassword(password);
        const hash = hashAndSalt.hash;
        const salt = hashAndSalt.salt;

        for (let i = 0; i < 10; i++) {

            let image = await Image.create({
                path: faker.image.avatar(),
            });

            let user = await User.create({
                email: faker.internet.email(),
                username: faker.name.firstName(),
                state: faker.address.state(),
                city: faker.address.city(),
                address: faker.address.streetName(),
                ZIPcode: faker.address.zipCode(),
                phone: faker.phone.phoneNumber(),
                salt: salt,
                hash: hash,
            });

            user.setImage(image);

        }

    } catch (e) {
        console.log(e + '!');   
    }
};

module.exports = seedUser;
