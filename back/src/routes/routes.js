const { Router } = require('express');
const path = require('path');
const multer = require('multer');
const storage = require("../config/files");
const passport = require("passport");
const setAuthorizationHeader = require('../middlewares/token');
const Moderator = require('../middlewares/Moderator');

const upload = multer({ storage: storage,
    // fileFilter: function (req, file, cb) {
    //     const ext = path.extname(file.originalname);
    //     if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
    //         return cb(new Error('Erro extensão não suportada!'), false);
    //     }
    //     cb(null, true);
    // },
    // limits:{
    //     fileSize: 2048 * 2048
    // }

});

const UserController = require('../controllers/UserController');
const CommentController = require('../controllers/CommentController');
const ProductController = require('../controllers/ProductController');
const AuthController = require("../controllers/AuthController");
const OrderController = require("../controllers/OrderController");

// const moderatorMiddleware = require("../middlewares/moderator");

const router = Router();

router.use("/private", passport.authenticate('jwt', {session: false})); 

//--User-Routes

router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users', UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);
router.post('/users/images/:id', upload.single('image'), UserController.addImageUser);

//--User-Routes

//------------------------------------X---------------------------------------//

//--Comment-Routes

router.get('/comments', CommentController.index);
router.get('/comments/:id', CommentController.show);
router.post('/comments', setAuthorizationHeader, CommentController.create);
router.put('/comments/:id', CommentController.update);
router.delete('/comments/:id', setAuthorizationHeader, Moderator, CommentController.destroy);
router.post('/comments/images/:id', upload.single('image'), CommentController.addImageComment);

//--Comment-Routes

//------------------------------------X---------------------------------------//

//--Product-Routes

router.get('/products', ProductController.index);
router.get('/products/:id', ProductController.show);
router.post('/products', setAuthorizationHeader, ProductController.create);
router.put('/products/:id', setAuthorizationHeader, ProductController.update);
router.delete('/products/:id', setAuthorizationHeader, Moderator, ProductController.destroy);
router.put('/favorite/:id', setAuthorizationHeader, ProductController.productUserFavorite);
router.put('/unfavorite/:id', setAuthorizationHeader, ProductController.removeFavorite);
router.get('/listFavorite/:id', setAuthorizationHeader, ProductController.listFavorite);
router.post('/products/images/:id', upload.single('image'), ProductController.addImageProduct);

//--Product-Routes

//------------------------------------X---------------------------------------//

//--Order-Routes

router.get('/orders', setAuthorizationHeader, OrderController.index);
router.get('/orders/:id', setAuthorizationHeader, OrderController.show);
router.post('/orders', setAuthorizationHeader, OrderController.create);
router.put('/orders/:id', OrderController.update);
router.delete('/orders/:id', OrderController.destroy);
router.get('/cart', OrderController.userCart);

//--Order-Routes

//------------------------------------X---------------------------------------//

//--Auth-Routes

router.post('/register', AuthController.register);
router.get('/private/getDetails', AuthController.getDetails);
router.post('/login', AuthController.login);


//--Auth-Routes

module.exports = router;
