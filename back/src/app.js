const express = require('express');
const cors = require('cors');

require('./config/dotenv')();
// require('./strategies/jwtStrategy')(passport);
require('./config/sequelize');

const app = express();
const routes = require('./routes/routes');
const port = process.env.PORT;
const path = require('path');
const passport = require("passport");

app.use(cors());
app.use(passport.initialize());
app.use(passport.session());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(routes);

app.use('/uploads', express.static(path.join(__dirname, '..', 'uploads'))); //servindo_estaticamente

require("./middlewares/jwtPassport")(passport); //autenticação
app.use(passport.initialize());


app.listen(port, () => {
  console.log(`${process.env.APP_NAME} app listening at http://localhost:${port}`);
});
    