const { response } = require('express');
const Order = require('../models/Order');
const Product = require('../models/Product');
const User = require('../models/User');
const Sequelize = require('../config/sequelize');


//CRUD_base--

const index = async(req,res) => {
    try {
        const order = await Order.findAll({include:{model: Product}});
        return res.status(200).json({order});
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const order = await Order.findByPk(id, {include: {model: Product}});
        return res.status(200).json({order});
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

const create = async(req,res) => {
    try{
          const order = await Order.create(req.body);

          const product = await Product.findByPk(req.body.ProductId);
          await order.setProduct(product);

          const user = await User.findByPk(req.body.UserId);
          await order.setUser(user);

          return res.status(201).json({order: order});
    }catch(err){
          res.status(500).json(err + '!');
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Order.update(req.body, {where: {id: id}});
        if(updated) {
            const order = await Order.findByPk(id);
            return res.status(200).send(order);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Order.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json(err + '!');
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

//CRUD_base--

//Cart

const userCart = async(req,res) => {
    try {
        const user = await User.findByPk(req.body.UserId);

        var orders = await Order.findAll({where: {UserId: user.id}, include:{model: Product}});
        
        const ordersAndPrice = ({
            ...orders,
            total: 0
        });

        orders.forEach(order => {
            ordersAndPrice.total += order.Product.shipping + order.amount*order.Product.pricetag;
        }); 

        return res.status(200).json(ordersAndPrice);

    }catch(err){
        return res.status(500).json(err + '!');
    }
};

//Cart

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    userCart,
};
