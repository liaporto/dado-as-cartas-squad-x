const { response } = require('express');
const User = require('../models/User');
const Image = require('../models/Image');
const Sequelize = require('../config/sequelize');
const path = require('path');
const Auth = require("../config/auth");
const mailer = require('../config/mail').mailer;

//CRUD_base--

const index = async(req,res) => {
    try {
        const users = await User.findAll({include:{model: Image}});
        return res.status(200).json({users});
    }catch(err){
        return res.status(500).json({err});
    }
};


const show = async(req,res) => {
    const {id} = req.params;
    console.log(id)
    try {
        const user = await User.findByPk(id, {include:{model: Image}});
        return res.status(200).json({user});
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

const create = async(req,res) => {
    try{
          const { password } = req.body;
          const hashAndSalt = Auth.generatePassword(password);
          const salt = hashAndSalt.salt;
          const hash = hashAndSalt.hash;
          const newUserData = {
            email: req.body.email,
            username: req.body.username,
            state: req.body.state,
            city: req.body.city,
            address: req.body.address,
            ZIPcode: req.body.ZIPcode,
            phone: req.body.phone,
            hash: hash,
            salt: salt,
          }

          const user = await User.create(newUserData);

          const message = {
              to: user.email,
              subject: "Bem-vindo ao Dado Às Cartas!",
              text: "Olá, nós do Dado Às Cartas passamos aqui para avisar que a sua conta foi criada com sucesso!"
        }

          mailer.sendMail(message, (err) => {
              console.log(err + "!");
          });

          return res.status(201).json({message: "Usuário cadastrado com sucesso!", user: user});   
    }catch(err){
          res.status(500).json({error: err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuario deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuario não encontrado.");
    }
};

//CRUD_base--

//upload_de_imagem

const addImageUser = async(req, res) => {
    const {id} = req.body;
    try {     
		const user = await User.findByPk(id, {include:{model: Image}});
        console.log(req.image);
		if(req.file){
			const path = process.env.APP_URL + "/uploads/" + req.file.filename;

			const image = await Image.create({
				path: path,
			});
			await user.setImage(image);
		}
		await user.reload();
		return res.status(200).json(user);
	} catch (e) {
		return res.status(500).json(e + "!");
	}
};

const removeImage = async(req, res) => {
	try {
		const {id} = req.params;
		const image  = await Image.findByPk(id);
		const pathDb = image.path.split("/").slice(-1)[0];
		await fsPromise.unlink(path.join(__dirname, "..", "..", "uploads", pathDb));
		await image.destroy();
		return res.status(200).json("Foto deletada com sucesso");
	} catch (e) {
		return res.status(500).json(e + "!");
	}
};

//upload_de_imagem

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addImageUser,
    removeImage,
};
