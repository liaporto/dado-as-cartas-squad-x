const { response } = require('express');
const Product = require('../models/Product');
const User = require('../models/User');
const Image = require('../models/Image');
const Sequelize = require('../config/sequelize');

//CRUD_base--

const index = async(req,res) => {
    try {
        const products = await Product.findAll();
        return res.status(200).json({products});
    }catch(err){
        return res.status(500).json(err + '!');
    }
};


const show = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id, {include:{model: Image}});
        return res.status(200).json({product});
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

const create = async(req,res) => {
    try{
          const product = await Product.create(req.body);

          const user = await User.findByPk(req.body.UserId);
          await product.setUser(user);

          return res.status(201).json({message: "Produto postado com sucesso!", product: product});
    }catch(err){
          res.status(500).json(err + '!');
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Product.update(req.body, {where: {id: id}});
        if(updated) {
            const product = await Product.findByPk(id);
            return res.status(200).send(product);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Product.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json('Deletado com sucesso!');
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

//CRUD_base--

//upload_de_imagem

const addImageProduct = async(req, res) => {
	const {id} = req.body;
    
    try {
		const product = await Product.findByPk(id, {include:{model: Image}});
		if(req.file){
			const path = process.env.APP_URL + "/uploads/" + req.file.filename;
			console.log("path");

			const image = await Image.create({
				path: path,
			});
			await product.setImage(image);
		}
		await product.reload();
		return res.status(200).json(product);
	} catch (e) {
		return res.status(500).json(e + "!");
	}
};

const removeImage = async(req, res) => {
	try {
		const {id} = req.params;
		const image  = await Image.findByPk(id);
		const pathDb = image.path.split("/").slice(-1)[0];
		await fsPromise.unlink(path.join(__dirname, "..", "..", "uploads", pathDb));
		await image.destroy();
		return res.status(200).json("Foto deletada com sucesso");
	} catch (e) {
		return res.status(500).json(e + "!");
	}
};

//upload_de_imagem

//relacionamentos_de_chave_estrangeira_produto-usuário

const productUserFavorite = async(req, res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(req.body.UserId, {
            include: [
                {model: Product, as:'UserFavoriting'}                
            ]
        });
        const favorite_product = await Product.findByPk(id);
        await user.addUserFavoriting(favorite_product);
        await user.reload();
        return res.status(200).json(user);
    } catch(e) {
        return res.status(500).json(e + "!");
    }
};

const removeFavorite = async(req, res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(req.body.UserId, {
            include: [
                {model: Product, as:'UserFavoriting'}                
            ]
        });
        const favorite_product = await Product.findByPk(id);
        await user.removeUserFavoriting(favorite_product);
        await user.reload();
        return res.status(200).json(user);
    } catch(e) {
        return res.status(500).json(e + "!");
    }
};

const listFavorite = async(req,res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id, {
            include: [
                {model: Product, as:'UserFavoriting'}                
            ]
        });

        const listFavorites = await user.getUserFavoriting();
        return res.status(200).json({listFavorites})
    } catch(e){
        return res.status(500).json(e + "!");
    }
};



//relacionamentos_de_chave_estrangeira_produto-usuário


module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addImageProduct,
    removeImage,
    productUserFavorite,
    removeFavorite,
    listFavorite,
};
