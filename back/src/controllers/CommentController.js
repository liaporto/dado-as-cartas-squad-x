const { response } = require('express');
const Comment = require('../models/Comment');
const Product = require('../models/Product');
const User = require('../models/User');
const Image = require('../models/Image');
const Sequelize = require('../config/sequelize');

//CRUD_base--

const index = async(req,res) => {
    try {
        const comments = await Comment.findAll({include:{model: Image}});
        return res.status(200).json({comments});
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id, {include:{model: Image}});
        return res.status(200).json({comment});
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

const create = async(req,res) => {
    try{
          const comment = await Comment.create(req.body);

          const user = await User.findByPk(req.body.UserId);
          await comment.setUser(user);

          const product = await Product.findByPk(req.body.ProductId);
          await comment.setProduct(product);


          return res.status(201).json({message: "Comentário postado com sucesso!", comment: comment});
    }catch(err){
          res.status(500).json(err + '!');
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Comment.update(req.body, {where: {id: id}});
        if(updated) {
            const comment = await Comment.findByPk(id);
            return res.status(200).send(comment);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Comment.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json(err + '!');
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json(err + '!');
    }
};

//CRUD_base--

//upload_de_imagem

const addImageComment = async(req, res) => {
	const {id} = req.body;
    try {
		const comment = await Comment.findByPk(id, {include:{model: Image}});
		if(req.file){
			const path = process.env.APP_URL + "/uploads/" + req.file.filename;
			console.log("path");

			const image = await Image.create({
				path: path,
			});
			await comment.setImage(image);
		}
		await comment.reload();
		return res.status(200).json(comment);
	} catch (e) {
		return res.status(500).json(e + "!");
	}
};

const removeImage = async(req, res) => {
	try {
		const {id} = req.params;
		const image  = await Image.findByPk(id);
		const pathDb = image.path.split("/").slice(-1)[0];
		await fsPromise.unlink(path.join(__dirname, "..", "..", "uploads", pathDb));
		await image.destroy();
		return res.status(200).json("Foto deletada com sucesso");
	} catch (e) {
		return res.status(500).json(e + "!");
	}
};

//upload_de_imagem

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addImageComment,
    removeImage
};
