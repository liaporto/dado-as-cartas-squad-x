const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");
// const { productUserFavorite } = require("../controllers/ProductController");

const User = sequelize.define('User', {
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },

    username: {
        type: DataTypes.STRING,
        allowNull: false
    },

    hash: {
        type: DataTypes.STRING,
        allowNull: false
    },

    salt: {
        type: DataTypes.STRING,
        allowNull: false
    },

    state: {
        type: DataTypes.STRING,
        allowNull: false
    },

    city: {
        type: DataTypes.STRING,
        allowNull: false
    },

    address: {
        type: DataTypes.STRING,
        allowNull: false
    },

    ZIPcode: {
        type: DataTypes.NUMBER,
        allowNull: false
    },

    phone: {
        type: DataTypes.NUMBER,
        allowNull: false
    },
    
    isModerator: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },

});

User.associate = function(models) {
    User.hasMany(models.Comment, {});
    User.hasMany(models.Product, {});
    User.hasOne(models.Image, {});
    User.hasMany(models.Order, {});
    User.belongsToMany(models.Product, {through: 'productUserFavorite', as: 'UserFavoriting'});
};

module.exports = User;
