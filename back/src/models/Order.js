const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Order = sequelize.define('Order', {
    amount: {
        type: DataTypes.NUMBER,
        allowNull: false
    },
});

Order.associate = function(models) {
    Order.hasOne(models.Product, {});
    Order.belongsTo(models.User, {});
};

module.exports = Order;
