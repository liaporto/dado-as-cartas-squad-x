const sequelize = require("../config/sequelize");
const DataTypes = require("sequelize");

const Comment = sequelize.define('Comment', {
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },

    rating: {
        type: DataTypes.NUMBER,
        allowNull: false
    }
},{

});

Comment.associate = function(models){
    Comment.belongsTo(models.User, {});
    Comment.belongsTo(models.Product, {});
    Comment.hasOne(models.Image, {})
};

module.exports = Comment;
