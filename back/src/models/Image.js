const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Image = sequelize.define("Image", {
    path:{
        type: DataTypes.STRING,
        allowNull: false
    }
});

Image.associate = function(models) {
    Image.belongsTo(models.User, {});
    Image.belongsTo(models.Comment, {});
    Image.belongsTo(models.Product, {})
};

module.exports = Image;
