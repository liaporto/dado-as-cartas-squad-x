const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Product = sequelize.define('Product', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    shipping: {
        type: DataTypes.NUMBER,
        allowNull: false
    },

    description: {
        type: DataTypes.STRING,
        allowNull: false
    },

    pricetag: {
        type: DataTypes.NUMBER,
        allowNull: false
    },

    brand: {
        type: DataTypes.STRING,
        allowNull: false
    },

    categories: {
        type: DataTypes.STRING,
        allowNull: false
    },

    rating: {
        type: DataTypes.NUMBER,
        allowNull: false
    },
    
    recommendedAge: {
        type: DataTypes.STRING,
        allowNull: false
    },

    playerNumber: {
        type: DataTypes.STRING,
        allowNull: false
    },

    mechanics: {
        type: DataTypes.STRING,
        allowNull: false
    },

    gameTime: {
        type: DataTypes.STRING,
        allowNull: false
    }

});

Product.associate = function(models) {
    Product.hasMany(models.Comment, {});
    Product.belongsTo(models.User, {});
    Product.hasOne(models.Image, {});
    Product.belongsTo(models.Order, {});
    Product.belongsToMany(models.User, {through: 'productUserFavorite', as: 'FavoritedProduct'});
};

module.exports = Product;   
