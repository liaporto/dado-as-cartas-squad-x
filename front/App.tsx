import 'react-native-gesture-handler';

import React from 'react';
import { useFonts } from 'expo-font';

import {RootSiblingParent} from 'react-native-root-siblings';

import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import AuthProvider from './src/contexts/auth';

//telas
import FavoritesEmpty from './src/pages/FavoritesEmpty';
import Favorites from './src/pages/Favorites';
import SearchResult from './src/pages/SearchResult';
import Category from './src/pages/Category';
import Register from './src/pages/Register';
import Login from './src/pages/Login';
import Home from './src/pages/Home';
import Product from './src/pages/Product';
import Comments from './src/pages/Comments';
import Profile from './src/pages/Profile';
import EditProfile from './src/pages/EditProfile';
import LoginOrRegister from './src/pages/LoginOrRegister';
import BoughtProducts from './src/pages/BoughtProducts';
import ProductsOnSale from './src/pages/ProductsOnSale';
import Cart from './src/pages/Cart';
import Tabs from './src/components/Tabs';



const Stack = createStackNavigator();

export default function App() {

  const [ loaded ] = useFonts({
    SourceSansLight: require('./assets/fonts/SourceSansPro-Light.ttf'),
    SourceSans: require('./assets/fonts/SourceSansPro-Regular.ttf'),
    SourceSansSemiBold: require('./assets/fonts/SourceSansPro-SemiBold.ttf'),
    SourceSansBold: require('./assets/fonts/SourceSansPro-Bold.ttf'),
  });

  if(!loaded){
    return null;
  }
//colocar rotas do login e cadastro
  return (
    <RootSiblingParent>
  
    <NavigationContainer>
      <AuthProvider>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="LoginOrRegister" component={LoginOrRegister} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Tabs" component={Tabs} options={{headerShown: false}} />
          <Stack.Screen name="Product" component={Product} />
          <Stack.Screen name="Comments" component={Comments} />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="EditProfile" component={EditProfile} />
          <Stack.Screen name="Favorites" component={Favorites} />
          <Stack.Screen name="Cart" component={Cart} />
          <Stack.Screen name="Category" component={Category} />
          <Stack.Screen name="BoughtProducts" component={BoughtProducts} />
          <Stack.Screen name="ProductsOnSale" component={ProductsOnSale} />
        </Stack.Navigator>
      </AuthProvider>
    </NavigationContainer> 

    </RootSiblingParent>
  );
}
