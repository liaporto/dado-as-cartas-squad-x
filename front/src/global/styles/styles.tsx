import styled from "styled-components/native"

export const AppView = styled.View`
  background-color: #102428;
  width: 100%;
  height: 100%;
`

export const StyledText = styled.Text`
  font-family: ${({theme}) => theme.fonts.normal};
  color: ${({theme}) => theme.colors.darkestGreenBlue}
`
