import styled from "styled-components/native";

export const AppView = styled.View`
  flex: 1;
  background-color: #102428;
`

export const StyledText = styled.Text`
  font-family: SourceSans;
  color: #102428;
`

export const StyledLinkText = styled.Text`
  font-family: SourceSansSemiBold;
  color: #fff;
  font-size: 16px;
  text-decoration: underline;
  text-decoration-color: #fff;
`

export const StyledPurpleButton = styled.TouchableOpacity`
  display: flex;
  width: 100%;
  height: 40%;
  align-items: center;
  justify-content: center;

  border-radius: 5px;

  background-color: #6E4CB2;
  box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.25);
`

export const StyledGreenButton = styled.TouchableOpacity`
  display: flex;
  width: 100%;
  height: 40%;
  align-items: center;
  justify-content: center;

  border-radius: 5px;

  background-color: #77B21F;
  box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.25);
`

export const StyledOrangeButton = styled.TouchableOpacity`
  display: flex;
  width: 100%;
  height: 40%;
  align-items: center;
  justify-content: center;

  border-radius: 5px;

  background-color: #FB873F;
  box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.25);
`

export const StyledButtonText = styled.Text`
  font-family: SourceSansSemiBold;
  color: #fff;
  font-size: 22px;
`

export const ScrollAppView = styled.ScrollView`
  background-color: #102428;
`