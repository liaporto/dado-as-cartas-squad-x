import api from '../api'

export default {
  async listComments(){
    try{
      const response = await api.get('/comments');
      return response;
    } catch (err){
      console.log(err);
    }
  },

  async showComments(commentId:number){
    try{
      const response = await api.get(`/comments/${commentId}`);
      return response;
    } catch(err){
      console.log(err);
    }
  },

  async postComment(data:any){
    try{
      const response = await api.post('/comments', data);
      return response;
    } catch(err){
      console.log(err);
    }
  },

  async deleteComment(commentId:number){
    try{
      const response = await api.delete(`/comments/${commentId}`);
      return response;
    } catch(err){
      console.log(err);
    }
  },
}