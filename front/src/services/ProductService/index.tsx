import api from '../api'

export default {
  async listProducts(){
    try{
      const response = await api.get('/products');
      return response;
    } catch (err){
      console.log(err);
    }
  },
  async showProduct(productId:number){
    try{
      const response = await api.get(`/products/${productId}`);
      return response;
    } catch(err){
      console.log(err);
    }
  },
  
  async setFavorite(data:any){
    try{
      const response = await api.put(`/favorite/${data.ProductId}`, data)
      return response;
    } catch(err){
      console.log(err);
    }
  },

  async removeFavorite(data:any){
    try{
      const response = await api.put(`/unfavorite/${data.ProductId}`, data)
      return response;
    } catch(err){
      console.log(err);
    }
  },

  async listFavorite(data:any){
    try{
      const response = await api.get(`/listFavorite/${data.UserId}`, data);
      return response;
    } catch(err){
      console.log(err);
    }
  }
}