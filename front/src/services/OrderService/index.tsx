import api from '../api'

export default {
  async createOrder(data:any){
    try{
      const response = await api.post(`/orders`, data);
      return response;
    } catch(err){
      console.log(err);
    }
  },

  async showCart(data:any){
    try{
      const response = await api.get('/cart', data);
      return response;
    } catch(err){
      console.log(err);
    }
  }
}