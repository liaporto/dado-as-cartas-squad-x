import api from '../api'

export default {
  async showUser(userId:number){
    try{
      const response = await api.get(`/users/${userId}`);
      return response;
    } catch(err){
      console.log(err);
    }
  }
}