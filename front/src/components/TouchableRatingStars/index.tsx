import React, {useState, useEffect} from 'react'

import {Text, TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { RatingStarsContainer } from './styles'

interface RatingStarsProps {
  iconSize?:number;
  handleStarPress: (starId:number) => void;
  starColors?:any;
}

const TouchableRatingStars = ({starColors, handleStarPress, iconSize}:RatingStarsProps) => {

  return (
    <RatingStarsContainer>
      <TouchableOpacity onPress={() => handleStarPress(0)}><Icon name="star" size={iconSize} color={starColors[0]}/></TouchableOpacity>
      <TouchableOpacity onPress={() => handleStarPress(1)}><Icon name="star" size={iconSize} color={starColors[1]}/></TouchableOpacity>
      <TouchableOpacity onPress={() => handleStarPress(2)}><Icon name="star" size={iconSize} color={starColors[2]}/></TouchableOpacity>
      <TouchableOpacity onPress={() => handleStarPress(3)}><Icon name="star" size={iconSize} color={starColors[3]}/></TouchableOpacity>
      <TouchableOpacity onPress={() => handleStarPress(4)}><Icon name="star" size={iconSize} color={starColors[4]}/></TouchableOpacity>
  </RatingStarsContainer>
  )
}

export default TouchableRatingStars
