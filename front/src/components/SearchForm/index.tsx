import React from 'react'

import { useForm, Controller } from 'react-hook-form'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { SearchContainer, SearchInput, TouchableSearchIcon } from './styles'

interface FormData{
  search: string;
}

const SearchForm = () => {
  
  const {control, handleSubmit} = useForm();

  const onSubmit = (data:FormData) =>{
    // TODO: Fazer a lógica para navegar pra resultados de pesquisa
    console.log(data.search);
  }

  const onError = (data:any) =>{
    console.log(data);
  }

  return (
    <SearchContainer>
      <Controller
        control={control}
        render={({field:{onBlur, onChange, value}})=>(
          <SearchInput
            placeholder="Pesquisar..."
            onBlur={onBlur}
            onChangeText={(value:any)=>{
              onChange(value)
            }}
            value={value}
          />
        )}
        name="search"
        defaultValue=""
      />
      <TouchableSearchIcon onPress={handleSubmit(onSubmit, onError)}>
        <Icon name="magnify" size={20}/>
      </TouchableSearchIcon>
    </SearchContainer>
  )
}

export default SearchForm
