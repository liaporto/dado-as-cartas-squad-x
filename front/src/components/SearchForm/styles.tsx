import styled from "styled-components/native";

export const SearchContainer = styled.View`
  flex: 1;
  display: flex;
  height: 35px;
  margin: 0 20px;
  padding: 0 10px;
  flex-direction: row;

  border-radius: 5px;

  background-color: #FFFFFF;
`

export const SearchInput = styled.TextInput`
  flex: 1;
  height: 35px;

  font-family: SourceSans;
`

export const TouchableSearchIcon = styled.TouchableOpacity`
  display: flex;
  height: 100%;
  justify-content: center;
`