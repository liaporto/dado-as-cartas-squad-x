import React, {useState, useEffect} from "react";

import { TouchableOpacity, View, Text, Modal, TextInput, Image} from "react-native";
import { useForm, Controller } from 'react-hook-form';

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import TouchableRatingStars from '../../components/TouchableRatingStars'

import {Camera} from 'expo-camera';

import {
  StyledButtonText,
  StyledLinkText,
  StyledGreenButton,
} from "../../global/styles";

import {
  MainModal,
  ButtonContainer,
  ModalInner,
  TouchableCloseButton,
  ModalTitle,
  TouchableOpenCamera,
  OpenCameraText,
  CameraContainer,
  CameraElement,
  TouchableCloseCamera,
  TouchableTakePicture,
  CommentTextInput,
  Error,
  TextContainer,
} from "./styles";

import logoImg from "../../../assets/Logo.png";

import CommentService from '../../services/CommentService'

interface ModalProps {
  isVisible: boolean;
  changeModalVisibility(visibility: boolean): any;
  user_id: number;
  product_id: number;
}

interface FormData{ 
  rating?: number;
  comment: string;
  photo?: {};
}

const ModalPostComment = ({ isVisible, changeModalVisibility, user_id, product_id }: ModalProps) => {

  {/* TODO: Ajustar para aparecer direito no navegador */}

  const { control, handleSubmit, formState:{errors} } = useForm({mode: 'onTouched'});

  // Usuário
  const [userId, setUserId] = useState(user_id);

  // Produto
  const[productId, setProductId] = useState(product_id);


  const [rating, setRating] = useState(0);
  const [starColors, setStarColors] = useState(["#818181", "#818181", "#818181", "#818181", "#818181"]);

  const handleStarPress = (starId:any) => {
    setRating(starId+1);
    setStarColors(starColors.map((star, index) => {
      if(index <= starId){
        return "#77B21F";
      } else {
        return "#818181";
      }
    }))
  }

  const onSubmit = (data:FormData) => {

    const postCommentData = {
      description: data.comment,
      rating: rating || 0,
      UserId: userId,
      ProductId: productId,
    }

    console.log(postCommentData);

    CommentService.postComment(postCommentData).then(postCommentResponse => {
      if(postCommentResponse){
        alert('Comentário postado com sucesso!');
        changeModalVisibility(!isVisible);
      }
    })
  }

  return (
    <MainModal
      animationType="fade"
      transparent={true}
      visible={isVisible}
      onRequestClose={() => {
        console.log("close");
        changeModalVisibility(!isVisible);
      }}
    >
      <ModalInner>
        <TouchableCloseButton onPress={() => changeModalVisibility(!isVisible)}>
          <Icon name="close" size={28} color="#FB873F" />
        </TouchableCloseButton>

        <TextContainer>
          <ModalTitle>O que você achou do jogo?</ModalTitle>
          <TouchableRatingStars iconSize={42} handleStarPress={handleStarPress} starColors={starColors}/>
        </TextContainer>

        <Controller
          control = {control}
          render = {({field:{onBlur, onChange, value}})=> (
            <CommentTextInput
              placeholder='Comentário...'
              onBlur={onBlur}
              onChangeText= {(value:any) => onChange(value)}
              value={value}
            />
          )}
          rules={{ 
            required: 'Você não escreveu nada!'
          }}

          name = 'comment'
          defaultValue = ''
        />
        {errors.comment && <Error>{errors.comment.message}</Error>}

        <ButtonContainer>
          <StyledGreenButton onPress ={handleSubmit(onSubmit)}>
            <StyledButtonText>Postar avaliação</StyledButtonText>
          </StyledGreenButton>
        </ButtonContainer>
      </ModalInner>

    </MainModal>
  );
};

export default ModalPostComment;
