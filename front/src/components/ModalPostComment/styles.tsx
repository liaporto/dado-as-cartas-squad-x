import styled from "styled-components/native";

import { Camera } from "expo-camera";

import {StyledText} from '../../global/styles'

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";


export const MainModal = styled.Modal`
  `

export const ModalInner = styled.View`
  height: ${hp('90%')};
  width: 90%;
  margin: auto;
  padding: 5% 10%;
  justify-content: space-between;
  align-items: center;
  
  border-width: 1px;
  border-color: #000;
  border-style: solid;

  border-radius: 10px;
  background-color: #fff;
`

export const TouchableCloseButton = styled.TouchableOpacity`
  display: flex;
  width: 100%;
  align-items: flex-end;
`

export const TextContainer = styled.View`
  height: 15%;
  justify-content: center;
  align-items: center;
  `

export const ModalTitle = styled.Text`
  text-align: center;
  color: #102428;
  font-size: 24px;
  font-family: SourceSansSemiBold;
`

export const ModalText = styled.Text`
  text-align: center;
  color: #102428;
  font-size: 16px;
  font-family: SourceSans;
`

export const TouchableOpenCamera = styled.TouchableOpacity`
  width: 100%;
  height: 100px;

  margin: 8% 0;

  align-items: center;
  justify-content: center;

  border-style: dashed;
  border-color: #77B21F;
  border-width: 2px;
  border-radius: 5px;
`

export const OpenCameraText = styled(StyledText)`
  color: #77B21F;
`

export const CameraContainer = styled.View`
  position: absolute;
  left: 0;
  top: 0;
  width: ${wp('100%')};
  height: ${hp('100%')};
  background-color: #102428;
`

export const CameraElement = styled(Camera)`
  width: 100%;
  height: 82%;
`

export const TouchableCloseCamera = styled.TouchableOpacity`
  position: absolute;
  top: 3%;
  right: 5%;
`

export const TouchableTakePicture = styled.TouchableOpacity`
  margin: auto;
  padding: 5%;
  width: ${wp('20%')};
  height: ${wp('20%')};
  border-radius: 100px;
  background-color: #777777cc;
  border-style: solid;
  border-width: 10px;
  border-color: #95959575;
`

export const CommentTextInput = styled.TextInput`
  height: 240px;
  padding: 0 5%;
  width: 100%;
  background-color: #F8F8F8;
`

export const Error = styled.Text `
  align-self: center;
  color: red;
`

export const ButtonContainer = styled.View`
  display: flex;
  width: 80%;
  height: 20%;
  align-items: center;
  justify-content: space-between;
`