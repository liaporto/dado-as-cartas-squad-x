import styled from 'styled-components/native'

import { StyledText, StyledButtonText } from '../../global/styles'

export const TileContainer = styled.View`
  flex: 1;

  min-height: 155px;
  width: 100%;
  margin: 5% 0;
  padding: 0 5%;

  flex-direction: row;
  align-items: center;

  border-radius: 5px;
  background-color: #fff;
`

export const ProductImgContainer = styled.View`
  height: 100%;
  width: 35%;
  margin-right: 5%;
  align-items: center;
  justify-content: center;
`

export const ProductImg = styled.Image`
  height: 100%;
  width: 100%;
`

export const InfoContainer = styled.View`
  flex: 1;
`

export const ProductInfoContainer = styled.View`
  height: 45%;
  justify-content: space-between;
  margin-bottom: 8%;
`

export const ProductTitle = styled(StyledText)`
  font-size: 16px;
  font-family: SourceSansBold;
`

export const ProductPrice = styled(StyledText)`
  font-size: 18px;
  font-family: SourceSansBold;
`

export const BuyingDateContainer = styled.View`
  width: 80%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

export const BuyingDateTitle = styled(StyledText)`
  font-family: SourceSansSemiBold;
`

export const BuyingDateInfo = styled(StyledText)`
  
`

export const TileButtonContainer = styled.View`
  height: 12%;
  width: 100%;

  align-items: flex-end;
`

export const TileButtonsContainer = styled.View`
  height: 12%;
  width: 100%;

  flex-direction: row;
  justify-content: flex-end;
`

export const TouchableGreenButtonFull = styled.TouchableOpacity`
  width: 50%;
  height: 100%;
  
  align-items: center;
  justify-content: center;
  flex-wrap: nowrap;
  
  border-radius: 5px;
  background-color: #77B21F;
  `
export const TouchableGreenButton = styled.TouchableOpacity`
  width: 40%;
  height: 100%;
  margin: 0 2.5%;
  
  align-items: center;
  justify-content: center;
  flex-wrap: nowrap;
  
  border-radius: 5px;
  background-color: #77B21F;
  `

export const TouchableRedButton = styled.TouchableOpacity`
  width: 40%;
  height: 100%;
  margin: 0 2.5%;

  align-items: center;
  justify-content: center;
  flex-wrap: nowrap;
  
  border-radius: 5px;
  background-color: #CD4040;
`

export const ButtonText = styled(StyledButtonText)`
  font-size: 14px;
`