import React, {useState} from 'react';

import { useNavigation } from '@react-navigation/native';

import {View, Text, Image, TouchableOpacity, Alert} from 'react-native';

import { TileContainer, ProductImgContainer, ProductImg, InfoContainer, ProductInfoContainer, ProductTitle, ProductPrice, BuyingDateContainer, BuyingDateTitle, BuyingDateInfo, TileButtonContainer, TileButtonsContainer, TouchableGreenButtonFull, TouchableGreenButton, TouchableRedButton, ButtonText} from './styles';

import placeholderImg from '../../../assets/products/product-placeholder-img.jpg'

interface ListedProductProps {
  type: string;
}

const ListedProductTile = ({type}:ListedProductProps) => {

  const navigation = useNavigation();
  const isModalVisible = true;
  const id = 1;

  const [typeOfTile, setTypeOfTile] = useState(type);

  const handleEditProduct = () => {
    // TODO: Implementar função
    console.log("edit");
  }

  const handleRemoveProduct = () => {
    // TODO: Implementar função
    return(Alert.alert(
      "Remover produto",
      "Tem certeza que deseja remover o produto?",
      [
        {
          text: "Não",
          onPress: () => console.log("Cancelado"),
        },
        {
          text: "Sim",
          onPress: () => console.log("Removido"),
        },
      ]
    ))
  }

  return (
    <TileContainer>
      <ProductImgContainer>
        <ProductImg source={placeholderImg} resizeMode="contain"/>
      </ProductImgContainer>
      <InfoContainer>
        <ProductInfoContainer>
          <ProductTitle>Nome do jogo</ProductTitle>
          <ProductPrice>R$500,00</ProductPrice>
          <BuyingDateContainer>
            <BuyingDateTitle>{type === "boughtProduct" ? "Data da compra " : "Publicado "}</BuyingDateTitle>
            <BuyingDateInfo>23/07/2021</BuyingDateInfo>
          </BuyingDateContainer>
        </ProductInfoContainer>
          {type === "boughtProduct" ?
          (
            <TileButtonContainer>
              <TouchableGreenButtonFull onPress={() => navigation.navigate("Comments", {id, isModalVisible})}>
                <ButtonText>Avaliar</ButtonText>
              </TouchableGreenButtonFull>
            </TileButtonContainer>
          )
          :
          (
            <TileButtonsContainer>
              <TouchableRedButton onPress={handleRemoveProduct}>
                <ButtonText>Excluir</ButtonText>
              </TouchableRedButton>
              <TouchableGreenButton onPress={handleEditProduct}>
                <ButtonText>Editar</ButtonText>
              </TouchableGreenButton>
            </TileButtonsContainer>
          )
        }
      </InfoContainer>
    
    </TileContainer>
  )
}

export default ListedProductTile
