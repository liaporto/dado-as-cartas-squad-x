import styled from 'styled-components/native'

import { StyledText, StyledButtonText } from '../../global/styles'

export const TileContainer = styled.View`
  flex: 1;

  min-height: 155px;
  width: 100%;
  margin: 5% 0;
  padding: 5%;

  flex-direction: row;
  align-items: flex-start;

  border-radius: 5px;
  background-color: #fff;
`

export const ProductImgContainer = styled.View`
  height: 100%;
  width: 35%;
  margin-right: 5%;
  align-items: center;
  justify-content: center;
`

export const ProductImg = styled.Image`
  height: 100%;
  width: 100%;
`

export const InfoContainer = styled.View`
  flex: 1;
`

export const ProductInfoContainer = styled.View`
  flex: 1;
  height: 100%;
  justify-content: space-evenly;
`

export const ProductTitle = styled(StyledText)`
  font-size: 16px;
  font-family: SourceSansBold;
`

export const ProductPrice = styled(StyledText)`
  font-size: 18px;
  font-family: SourceSansBold;
`

export const BuyingDateContainer = styled.View`
  
`

export const BuyingDateTitle = styled(StyledText)`
  font-family: SourceSansSemiBold;
`

export const BuyingInfo = styled(StyledText)`
  font-size: 12px;
`

export const ButtonText = styled(StyledButtonText)`
  font-size: 14px;
`