import React, {useState} from 'react';

import { useNavigation } from '@react-navigation/native';

import {View, Text, Image, TouchableOpacity, Alert} from 'react-native';

import { TileContainer, ProductImgContainer, ProductImg, InfoContainer, ProductInfoContainer, ProductTitle, ProductPrice, BuyingDateContainer, BuyingDateTitle, BuyingInfo, TileButtonContainer, TileButtonsContainer, TouchableGreenButtonFull, TouchableGreenButton, TouchableRedButton, ButtonText} from './styles';

import placeholderImg from '../../../assets/products/product-placeholder-img.jpg'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

interface ListedProductProps {
  type: string;
}

const CartProductTile = () => {

  const navigation = useNavigation();
  const isModalVisible = true;
  const id = 1;


  const handleRemoveProduct = () => {
    // TODO: Implementar função
    return(Alert.alert(
      "Remover produto",
      "Tem certeza que deseja remover o produto do carrinho?",
      [
        {
          text: "Não",
          onPress: () => console.log("Cancelado"),
        },
        {
          text: "Sim",
          onPress: () => console.log("Removido"),
        },
      ]
    ))
  }

  return (
    <TileContainer>
      <ProductImgContainer>
        <ProductImg source={placeholderImg} resizeMode="contain"/>
      </ProductImgContainer>
        <ProductInfoContainer>
          <ProductTitle>Nome do jogo</ProductTitle>
          <ProductPrice>R$500,00</ProductPrice>
          <BuyingInfo>Vendido e entregue por Loja 1</BuyingInfo>
        </ProductInfoContainer>
      <TouchableOpacity onPress={() => handleRemoveProduct()}>
        <Icon name="close" size={20} color="#FB873F"/>
      </TouchableOpacity>
    
    </TileContainer>
  )
}

export default CartProductTile
