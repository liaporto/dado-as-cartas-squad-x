import styled from "styled-components/native";

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

export const ModalInner = styled.View`
  height: 70%;
  width: 90%;
  margin: auto;
  padding: 10% 10%;
  justify-content: flex-start;
  align-items: center;

  border-radius: 10px;
  background-color: #053742;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
`

export const TouchableCloseButton = styled.TouchableOpacity`
  display: flex;
  width: 100%;
  align-items: flex-end;
`

export const LogoImage = styled.Image`
  height: 25%;
  margin-bottom: 5%;
`

export const TextContainer = styled.View`
  height: 10%;
  margin: 10% 0;
  justify-content: center;
  `

export const ModalTitle = styled.Text`
  text-align: center;
  color: #fff;
  font-size: 24px;
  font-family: SourceSans;
`

export const ModalText = styled.Text`
  text-align: center;
  color: #fff;
  font-size: 16px;
  font-family: SourceSans;
`

export const ButtonContainer = styled.View`
  width: 60%;
  height: 30%;
  margin: 10% 0;
  align-items: center;
  justify-content: space-between;
`

export const LinkContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: space-around;
  height: 12%;
`