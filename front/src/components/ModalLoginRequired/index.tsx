import React from "react";
import {useNavigation} from '@react-navigation/native'
import { TouchableOpacity, View, Text, Modal } from "react-native";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import {
  StyledButtonText,
  StyledLinkText,
  StyledPurpleButton,
} from "../../global/styles";

import {
  ButtonContainer,
  LogoImage,
  ModalInner,
  TouchableCloseButton,
  ModalTitle,
  ModalText,
  TextContainer,
} from "./styles";

import logoImg from "../../../assets/Logo.png";

interface ModalProps {
  isVisible: boolean;
  changeModalVisibility(visibility: boolean): any;
}

const LoginPrompt = ({ isVisible, changeModalVisibility }: ModalProps) => {
  const navigation = useNavigation();
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={isVisible}
      onRequestClose={() => {
        console.log("close");
        changeModalVisibility(!isVisible);
      }}
  >
      <ModalInner>
        <TouchableCloseButton onPress={() => changeModalVisibility(!isVisible)}>
          <Icon name="close" size={20} color="#fff" />
        </TouchableCloseButton>

        <LogoImage source={logoImg} resizeMode="contain" />

        <TextContainer>
          <ModalTitle>Você não está logado :(</ModalTitle>
          <ModalText>
            Para acessar esta ferramenta entre ou faça seu cadastro no
            aplicativo.
          </ModalText>
        </TextContainer>

        <ButtonContainer>
          <StyledPurpleButton onPress={()=> navigation.navigate("Login")}>
            <StyledButtonText>Entrar</StyledButtonText>
          </StyledPurpleButton>

          <StyledPurpleButton onPress={()=> navigation.navigate("Register")}>
            <StyledButtonText>Cadastrar</StyledButtonText>
          </StyledPurpleButton>
        </ButtonContainer>
      </ModalInner>
    </Modal>
  );
};

export default LoginPrompt;
