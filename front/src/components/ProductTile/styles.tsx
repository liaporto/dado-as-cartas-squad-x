import styled from "styled-components/native";

import { StyledText } from "../../global/styles";

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

export const TouchableCard = styled.TouchableOpacity`
  width: ${wp('40%')};
  height: ${hp('40%')};
  margin: 10px;
`

export const Card = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: #FFFFFF;
  padding: 15px;
  border-radius: 5px;
  box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1);
`

export const TouchableHeart = styled.TouchableOpacity`
  align-self: flex-end;
`

export const ImageContainer = styled.View`
  width: 100%;
  height: 50%;
  padding: 15px;
`

export const ProductImage = styled.Image`
  width: 100%;
  height: 100%;
`

export const ProductInformation = styled.View`
  flex: 1;
  width: 100%;
  justify-content: space-evenly;
`

export const ProductTitle = styled(StyledText)`
  font-family: SourceSansBold;
  font-size: 16px;
  align-self: center;
`

export const ProductDetails = styled.View`
  height: 60%;
  display: flex;
  justify-content: space-between;
`

export const RatingStarsContainer = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
`

export const ProductPrice = styled(StyledText)`
  font-family: SourceSansBold;
  font-size: 18px;
`

export const ProductCategories = styled(StyledText)`
  font-family: SourceSansLight;
  font-size: 10px;
`