import React, { useState, useEffect } from "react";
import { useNavigation } from "@react-navigation/native";

import placeholderImg from "../../../assets/products/product-placeholder-img.jpg";

import {
  TouchableCard,
  Card,
  ProductImage,
  TouchableHeart,
  ProductTitle,
  ProductPrice,
  ImageContainer,
  ProductInformation,
  ProductDetails,
  ProductCategories,
} from "./styles";

import RatingStars from "../RatingStars";

interface ProductTileProps{
  data: {
    id: number;
    name: string;
    pricetag: number;
    rating: number;
    categories: string;
    Image: {
      path: string;
    };
  }
}

const ProductTile = ({data}:ProductTileProps) => {

  const navigation = useNavigation();

  const handleNavigateToProduct = (id:number) => {
    navigation.navigate("Product", {id});
  }

  return (
    <TouchableCard onPress={() => {handleNavigateToProduct(data.id)}}>
      <Card>

        <ImageContainer>
          <ProductImage resizeMode="contain" source={placeholderImg} />
        </ImageContainer>

        <ProductInformation>
          <ProductTitle>{data.name}</ProductTitle>

          <ProductDetails>
            <RatingStars iconSize={15} rating={data.rating}/>
            <ProductPrice>R${data.pricetag.toFixed(2).replace(".", ",")}</ProductPrice>
            <ProductCategories>Categorias: {data.categories}</ProductCategories>
          </ProductDetails>
        </ProductInformation>
      </Card>
    </TouchableCard>
  );
};

export default ProductTile;
