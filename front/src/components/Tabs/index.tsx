import React, { useContext } from 'react'

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import Icon from 'react-native-vector-icons/SimpleLineIcons'
import {default as MaterialIcon} from 'react-native-vector-icons/MaterialCommunityIcons'

import Home from '../../pages/Home'
import CategoriesList from '../../pages/CategoriesList'
import Cart from '../../pages/Cart'
import Favorites from '../../pages/Favorites';
import FavoritesEmpty from '../../pages/FavoritesEmpty'; 

import BoughtProducts from '../../pages/BoughtProducts';
import { AuthContext } from '../../contexts/auth';
import Profile from '../../pages/Profile';

const Tab = createBottomTabNavigator();


const Tabs = () => {
  const Auth = useContext(AuthContext);
  return (
    <Tab.Navigator
    tabBarOptions={{
        activeTintColor:"#FB873F",
        inactiveTintColor:"#D3EBF8",
        showLabel: false,
        tabStyle:{
          backgroundColor: "#162E33"
        },
        style: {
          borderTopWidth: 0,
          backgroundColor: "#162E33"
        },
      }}
    >
      
      <Tab.Screen name="Home" component={Home}
        options={{tabBarIcon: ({focused=true, color, size}) =>{
          return <Icon name="home" color={color} size={25}/>
          }
        }}
      />
      <Tab.Screen name="Categorias" component={CategoriesList}
        options={{tabBarIcon: ({focused=false, color, size}) =>{
          return <Icon name="list" color={color} size={25}/>
          }
        }}
      />

    {
      Auth.signed == true && 

      <Tab.Screen name="Carrinho" component={Cart}
        options={{tabBarIcon: ({focused=false, color, size}) =>{
          return <MaterialIcon name="cart-outline" color={color} size={25}/>
          }
        }}
      />
    }

      <Tab.Screen name="Favoritos" component={Favorites}
        options={{tabBarIcon: ({focused=false, color, size}) =>{
          return <Icon name="heart" color={color} size={25}/>
          }
        }}
      />
      
      
      <Tab.Screen name="Perfil" component={Profile}
        options={{tabBarIcon: ({focused=false, color, size}) =>{
          return <Icon name="user" color={color} size={25}/>
          }
        }}
      />

    </Tab.Navigator>
  
  )}

export default Tabs
