import React, {useState, useContext, useEffect} from 'react'
import { View, Text, TouchableOpacity, Image, Alert } from 'react-native'

import { useNavigation } from '@react-navigation/native'

import {AuthContext} from '../../contexts/auth'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import RatingStars from '../RatingStars'

import profileImg from '../../../assets/profile/perfil-placeholder.png'
import commentImg from '../../../assets/img-comentario-placeholder.png'

import {CommentContainer, ProfileImage, CommentData, CommentUser, CommentDetails, CommentIcons, RatingsContainer, RatingsText, CommentText, CommentImage} from './styles'

import CommentService from '../../services/CommentService'
import UserService from '../../services/UserService'

interface CommentProps {
  data: {
    id: number;
    rating: number;
    UserId: number;
    ProductId: number;
    description: string;
  }
}

const Comment = ({data}:CommentProps) => {

  const Auth = useContext(AuthContext);
  const navigation = useNavigation();

  const [isLoggedIn, setIsLoggedIn] = useState<boolean>();

  const [username, setUsername] = useState();
  const [profilePicture, setProfilePicture] = useState();
  const [commentContent, setCommentContent] = useState();
  const [rating, setRating] = useState(data.rating);

  useEffect(()=> {
    UserService.showUser(data.UserId).then(res => {
      if(res){
        setUsername(res.data.user.username);
        if(res.data.user.Image){
          setProfilePicture(res.data.user.Image.path);
        }
      }
    }).catch(err => console.log(err));

    if(Auth.signed){
      setIsLoggedIn(true);
    }
  }, [])

  const handleDeleteComment = (commentId:number) => {
    if(true){
     return(Alert.alert(
      "Remover comentário",
      "Tem certeza que deseja remover o comentário?",
      [
        {
          text: "Não",
          onPress: () => console.log("Cancelado"),
        },
        {
          text: "Sim",
          onPress: () => {
            CommentService.deleteComment(commentId).then(res => {
              if(res){
                navigation.navigate("Comments");
                console.log("Removido");
              }
            }).catch(err => console.log(err));
          },
        },
      ]
    )) 
    }
  }

  return (
    <CommentContainer>
      <ProfileImage source={profileImg} resizeMode="contain"/>
      <CommentData>
        <CommentDetails>
          <CommentUser>{username}</CommentUser>
          <CommentIcons>
            <RatingsContainer>
              <RatingStars iconSize={15} rating={data.rating}/>
              <RatingsText>({data.rating})</RatingsText>
            </RatingsContainer>
            {isLoggedIn &&
              <TouchableOpacity onPress={()=> handleDeleteComment(data.id)}>
                <Icon name="delete-outline" size={20} color="#CD4040"/>
              </TouchableOpacity>
            }
            {/* <TouchableOpacity onPress={()=> handleDeleteComment(data.id)}>
              <Icon name="delete-outline" size={20} color="#CD4040"/>
            </TouchableOpacity> */}
          </CommentIcons>
        </CommentDetails>
        <CommentText>{data.description}</CommentText>
      </CommentData>
    </CommentContainer>
  )
}

export default Comment
