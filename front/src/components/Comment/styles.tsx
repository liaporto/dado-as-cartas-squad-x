import styled from 'styled-components/native'
import {StyledText} from '../../global/styles'

export const CommentContainer = styled.View`
  flex: 1;
  width: 100%;
  margin: 5% 0;
  flex-direction: row;
  align-items: flex-start;
`

export const ProfileImage = styled.Image`
  width: 16%;
  min-height: 40px;
  height: 30%;
  margin-right: 5%;
`

export const CommentData = styled.View`
  flex: 1;
`

export const CommentDetails = styled.View`
  width: 100%;
  margin-bottom: 1%;
  flex-direction: row;
  justify-content: space-between;
`

export const CommentUser = styled(StyledText)`
  font-family: SourceSansSemiBold;
`

export const CommentIcons = styled.View`
  width: 50%;
  flex-direction: row;
  justify-content: space-between;
`

export const RatingsContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
`

export const RatingsText = styled(StyledText)`
  font-size: 14px;
  color: #77B21F;
  font-family: SourceSansSemiBold;
`

export const CommentText = styled(StyledText)`
  margin-bottom: 2%;
  text-align: justify;
`

export const CommentImage = styled.Image`
  flex: 1;
  width: 100%;
  max-height: 70%;
  border-radius: 10px;
`