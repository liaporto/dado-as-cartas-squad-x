import styled from 'styled-components/native';

import { StyledText } from '../../global/styles';


export const RatingStarsContainer = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-items: center;
`

export const RatingText = styled(StyledText)`
  color: #77B21F;
`