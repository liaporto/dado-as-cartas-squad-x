import React, {useState, useEffect} from 'react'

import {Text} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { RatingStarsContainer } from './styles'

interface RatingStarsProps {
  iconSize:number;
  rating: number;
}

const RatingStars = ({iconSize, rating}:RatingStarsProps) => {
  const [remainingStars, setRemainingStars] = useState(0);

  useEffect(()=>{
    setRemainingStars(5 - rating);
  }, [])

  return (
    <RatingStarsContainer>
      {[...Array(rating)].map((star, index)=> {
        return <Icon key={index} name="star" size={iconSize} color="#77B21F"/>
      })}
      {remainingStars > 0 && [...Array(remainingStars)].map((star, index)=> {
        return <Icon key={index} name="star" size={iconSize} color="#818181"/>
      })}
  </RatingStarsContainer>
  )
}

export default RatingStars
