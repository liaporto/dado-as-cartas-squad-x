import styled from 'styled-components/native'

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen'

import { StyledText } from '../../global/styles'

export const TouchableCategory = styled.TouchableOpacity`
  height: ${wp('41%')};
  width: 48%;
  max-width: ${wp('41%')};
  margin: 2%;
  
  flex-basis: 50%;
  
  border-radius: 25px;
`

export const InnerContainer = styled.View`
  flex: 1;
  padding: 10% 2% 15% 10%;
  justify-content: space-between;
`

export const CategoryTitle = styled(StyledText)`
  font-size: 18px;
  font-family: SourceSansSemiBold;
`

export const CategoryImage = styled.Image`
  height: 65%;
  width: 65%;
  align-self: flex-end;
`