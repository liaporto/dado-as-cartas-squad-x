import React from 'react'
import {View, Text, Image} from 'react-native'

import { useNavigation } from '@react-navigation/native';

import {LinearGradient} from 'expo-linear-gradient';

import {TouchableCategory, InnerContainer, CategoryTitle, CategoryImage} from './styles'

interface CategoryTileProps {
  id: number;
  name: string;
  image: any;
  backgroundColor?: string;
  gradientStartColor: string;
  gradientEndColor: string;
}

const CategoryTile = ({id, name, image, gradientStartColor, gradientEndColor, backgroundColor}:CategoryTileProps) => {
  const navigation = useNavigation();
  return (
    <TouchableCategory onPress={()=> navigation.navigate("Category", {id, name})} style={backgroundColor ? {backgroundColor: backgroundColor} : {}}>
        <LinearGradient
          // Background Linear Gradient
          colors={[gradientStartColor, gradientEndColor]}
          start={{x: 0, y: 0}}
          end={{x: 0, y: 1}}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: '100%',
            borderRadius: 25
          }}
          />
        <InnerContainer>
          <CategoryTitle>{name}</CategoryTitle>
          <CategoryImage source={image} resizeMode="contain"/>
      </InnerContainer>
    </TouchableCategory>
  )
}

export default CategoryTile