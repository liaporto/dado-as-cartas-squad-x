import styled from "styled-components/native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

export const HeaderContainer = styled.View`
  display: flex;
  width: ${wp('100%')}px;
  height: ${hp('12%')}px;
  padding: 5% 5% 0;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  
  background-color: #FB873F;
`

export const TouchableLogo = styled.TouchableOpacity`
  display: flex;
  height: 50%;
  flex-basis: 10%;
  justify-content: center;
`

export const Logo = styled.Image`
  width: 100%;
  height: 100%;
`