import React from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import SearchForm from '../SearchForm'

import logo from '../../../assets/Logo.png'

import { HeaderContainer, TouchableLogo, Logo} from './styles'

const Header = () => {

  const handleLogoPress = () => {
    console.log("Pressed!")

    // TODO: Fazer a lógica para navegar pra Home
  }

  return (
    <HeaderContainer>
      <TouchableLogo onPress={handleLogoPress}>
        <Logo source={logo} resizeMode="contain"/>
      </TouchableLogo>
      <SearchForm/>
      <TouchableOpacity>
        <Icon name="dice-5-outline" size={28} color="#FFFFFF"/>
      </TouchableOpacity>
    </HeaderContainer>
  )
}

export default Header
