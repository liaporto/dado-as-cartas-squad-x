import styled from 'styled-components/native'

import { StyledText, AppView } from '../../global/styles'

export const CategoriesAppView = styled(AppView)`
  flex: 1;
  justify-content: center;
  align-items: center;
`

export const CategoriesScrollView = styled.ScrollView`
`

export const Body = styled.View`
  width: 100%;
  padding: 5%;
`

export const CategoriesTitle = styled(StyledText)`
  margin: 5% 0 10%;

  color: #fff;
  font-size: 30px;
  font-family: SourceSansSemiBold;
`

export const CategoriesContainer = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
`