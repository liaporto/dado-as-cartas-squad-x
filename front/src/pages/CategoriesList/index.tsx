import React from 'react';
import {View, ScrollView, Text, Image} from 'react-native'

import { AppView } from '../../global/styles';

import Header from '../../components/Header'
import CategoryTile from '../../components/CategoryTile';

import cartasImg from '../../../assets/categories/cartas.png'
import miniaturasImg from '../../../assets/categories/miniaturas.png'
import familiaImg from '../../../assets/categories/familia.png'
import estrategiaImg from '../../../assets/categories/estrategia.png'
import rpgImg from '../../../assets/categories/rpg.png'
import cooperativoImg from '../../../assets/categories/cooperativo.png'
import infantilImg from '../../../assets/categories/infantil.png'
import comediaImg from '../../../assets/categories/comedia.png'


import { CategoriesAppView, CategoriesScrollView, Body, CategoriesContainer, CategoriesTitle } from './styles';

const CategoriesList = () => {

  return (
    <CategoriesAppView>
      <CategoriesScrollView>
        <Header/>
        <Body>
          <CategoriesTitle>Categorias</CategoriesTitle>

          <CategoriesContainer>
            <CategoryTile
              id={1}
              name="Cartas"
              gradientStartColor="#6fddff"
              gradientEndColor="#127897"
              image={cartasImg}
            />
            <CategoryTile
              id={2}
              name="Miniaturas"
              gradientStartColor="#ff9e61"
              gradientEndColor="#c85f1d"
              image={miniaturasImg}
            />
            <CategoryTile
              id={3}
              name="Família"
              gradientStartColor="#a492ff"
              gradientEndColor="#604ace"
              image={familiaImg}
            />
            <CategoryTile
              id={4}
              name="Estratégia"
              gradientStartColor="#8fcc33"
              gradientEndColor="#445f1a"
              image={estrategiaImg}
            />
            <CategoryTile
              id={5}
              name="RPG"
              gradientStartColor="#3787CC"
              gradientEndColor="#273F54"
              image={rpgImg}
            />
            <CategoryTile
              id={6}
              name="Cooperativo"
              gradientStartColor="#249eb9"
              gradientEndColor="#0e3b46"
              image={cooperativoImg}
            />
            <CategoryTile
              id={7}
              name="Infantil"
              gradientStartColor="#92FFE5"
              gradientEndColor="#1E7153"
              image={infantilImg}
            />
            <CategoryTile
              id={8}
              name="Comédia"
              gradientStartColor="#B395ED"
              gradientEndColor="#604791"
              image={comediaImg}
            />
          </CategoriesContainer>
        </Body>
      </CategoriesScrollView>
    </CategoriesAppView>
  )
}

export default CategoriesList