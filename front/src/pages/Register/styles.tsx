import { TextInputMask } from 'react-native-masked-text';
import RNPickerSelect from 'react-native-picker-select';
import styled from 'styled-components/native';


export const Body = styled.ScrollView`
flex: 1;
background-color: #102024;
`
export const Arrow = styled.TouchableOpacity`
padding-top: 15%;
padding-left: 8%;
`

export const Title = styled.Text `
font-family: SourceSans;
font-weight: normal;
font-size: 36px;
color: #77B21F;
align-self: center;
padding-top:3%;
`

export const Subtitle = styled.Text `
font-family: SourceSans;
font-weight: normal;
font-size: 18px;
color: #FAFDFF;
align-self: center;
padding-top:5%;
padding-bottom:1%;
`


export const MyTextInputMask = styled(TextInputMask)`
    font-family: SourceSans;
    font-weight: normal;
    font-size: 10px;
    padding-left: 5%;
    margin: 3%;
    align-self: center;
    background-color: #FAFDFF;
    width: 321px;
    height: 50px;
    border-radius: 5px;
`
export const MyImage = styled.Image `
    width: 140px;
    height: 148px;
    margin: auto;
    margin-top: 10%;
`

export const MyTextInput = styled.TextInput `
    font-family: SourceSans;
    font-weight: normal;
    font-size: 10px;
    padding-left: 5%;
    margin: 3%;
    align-self: center;
    background-color: #FAFDFF;
    width: 321px;
    height: 50px;
    border-radius: 5px;
`


export const Error = styled.Text `
    font-family: SourceSans;
    font-weight: normal;
    margin-left: 9%;
    color: red;
`

export const Entrar = styled.TouchableOpacity`
    align-self: center;
    background-color: #77B21F;
    height: 40px;
    width: 227px;
    border-radius: 5px;
    margin-bottom: 10%;
`

export const Cadastrar = styled.Text`
    text-align: center;
    font-family: SourceSans;
    font-weight: bold;
    font-size: 16px; 
    color: #FAFDFF;
    margin-top: 10px;
`




export const MyText = styled.Text`
    font-family: SourceSans;
    font-weight: normal;
    color: #FAFDFF;
    padding-left: 10%;
`