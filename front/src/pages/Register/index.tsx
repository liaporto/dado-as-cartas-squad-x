import React from 'react';
import {View, TextInput, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import { useForm, Controller } from 'react-hook-form';
import {Title, Subtitle, MyImage , MyTextInput, MyTextInputMask, Error , Entrar, Cadastrar, Arrow, MyText,Body } from './styles';
import logo from '../../../assets/logo/logo.png';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {TextInputMask} from 'react-native-masked-text';
import RNPickerSelect,{defaultStyles} from 'react-native-picker-select';
import { useNavigation } from '@react-navigation/native';
import api from '../../services/api';

interface RegisterData{
  username: string;
  email: string;
  password: string;
  phone:string;
  ZIPcode: string;
  address: string;
  state: string;
  city: string;
 }

export default function Register () {

    const { control, handleSubmit, formState:{errors}, getValues } = useForm({mode: 'onTouched'});
  
    const onSubmit = async (data:RegisterData) => { 
        

        console.log(data)
        await api.post('/users', data).then(response => {
            console.log(response);
            alert('Cadastro feito com sucesso!')
            navigation.navigate('Login')
        },(error) => {          
          console.log(error);
       
        })
      }   


    const onError = (errors: Object) => { console.log(errors) };
    const navigation = useNavigation ()
       
     
return (
  <Body>
      <Arrow onPress={()=> navigation.navigate("LoginOrRegister")}>
      <Icon name="arrow-left" color="#ffffff" size={28}/>
      </Arrow>

      <MyImage source={logo} resizeMode='contain'/>
      <Title>Cadastro</Title>
      <Subtitle>Primeiro informe seus dados pessoais:</Subtitle>


       <View>
           <Controller
            control = {control}
            render = {({field:{onBlur, onChange, value}})=> (

              <MyTextInput
                 placeholder='Nome Completo'
                 onBlur={onBlur}
                 onChangeText= {(value:any) => onChange(value)}
                 value={value}
              />
              
            )}
            rules={{ 
              required: 'O nome completo é obrigatório.',
          }}
 
            name = 'username'
            defaultValue = ''
           />
           {errors.nome && <Error>{errors.nome.message}</Error>}

       </View>

       <View>
           <Controller
            control = {control}
            render = {({field:{onBlur, onChange, value}})=> (
              <MyTextInput
                 placeholder='E-mail'
                 onBlur={onBlur}
                 onChangeText= {(value:any) => onChange(value)}
                 value={value}
              />
            )}
            rules={{ 
              required: 'O e-mail é obrigatório.',
               pattern: {
                   value: /^\S+@\S+$/i,
                 message: 'Formato de e-mail inválido.'
               },
          }}
 
            name = 'email'
            defaultValue = ''
           />
            {errors.email && <Error>{errors.email.message} </Error>}

       </View>


       <View>
           <Controller
            control = {control}
            render = {({field:{onBlur, onChange, value}})=> (
              <MyTextInput
                 placeholder='Senha'
                 secureTextEntry
                 onBlur={onBlur}
                 onChangeText= {(value:any) => onChange(value)}
                 value={value}
              />
            )}

            rules={{ 
              required: 'A senha é obrigatória.',
          }}
 
            name = 'password'
            defaultValue = ''
           />
           {errors.senha && <Error>{errors.senha.message}</Error>}

       </View>

       <View>
           <Controller
            control = {control}
            render = {({field:{onBlur, onChange, value}})=> (
              <MyTextInput
                 placeholder='Telefone'
                 onBlur={onBlur}
                 onChangeText= {(value:any) => onChange(value)}
                 value={value}
              />
            )}
           
            name = 'phone'
            defaultValue = ''
           />
  </View>

  <Subtitle>Agora informe seu endereço:</Subtitle>

  <View>
           <Controller
            control = {control}
            render = {({field:{onBlur, onChange, value}})=> (
  
              <MyTextInputMask
                 type={'zip-code'}
                 placeholder={'CEP'}
                 onBlur={onBlur}
                 value={value}
                 onChangeText= {(value:any) => onChange(value)}
              />
              
            )}

            rules={{ 
              required: 'O CEP é obrigatório.',
          }}

            name = 'ZIPcode'
            defaultValue = ''
           />

           {errors.cep && <Error>{errors.cep.message}</Error>}
       </View>


       <View>
           <Controller
            control = {control}
            render = {({field:{onBlur, onChange, value}})=> (
              <MyTextInput
                 placeholder='Rua e número'
                 onBlur={onBlur}
                 onChangeText= {(value:any) => onChange(value)}
                 value={value}
              />
            )}

            rules={{ 
              required: 'Rua e número são obrigatórios.',
            }}
 
            name = 'address'
            defaultValue = ''
           />
           {errors.rua && <Error>{errors.rua.message}</Error>}
       </View>

      

       <MyText>Selecione um Estado</MyText>
       <View>
           
           <Controller
            control = {control}
            render = {({field:{onBlur, onChange, value}})=> (
              
          <RNPickerSelect 
              style={defaultStyles.modalViewMiddle}
              value={value}
              onValueChange={(value:any) => onChange(value)}
              items={[
          { label: 'Acre', value: 'AC' },
          { label: 'Alagoas', value: 'AL' },
          { label: 'Amapá', value: 'AP' },
          { label: 'Amazonas', value: 'AM' },
          { label: 'Bahia', value: 'BA' },
          { label: 'Ceará', value: 'CE' },
          { label: 'Distrito Federal', value: 'DF' },
          { label: 'Espírito Santo', value: 'ES' },
          { label: 'Goiás', value: 'GO' },
          { label: 'Maranhão', value: 'MA' },
          { label: 'Mato Grosso', value: 'MT' },
          { label: 'Minas Gerais', value: 'MG' },
          { label: 'Pará', value: 'PA' },
          { label: 'Paraíba', value: 'PB' },
          { label: 'Paraná', value: 'PR' },
          { label: 'Pernambuco', value: 'PE' },
          { label: 'Piauí', value: 'PI' },
          { label: 'Rio de Janeiro', value: 'RJ' },
          { label: 'Rio Grande do Norte', value: 'RN' },
          { label: 'Rio Grande do Sul', value: 'RS' },
          { label: 'Rondônia', value: 'RO' },
          { label: 'Roraima', value: 'RR' },
          { label: 'Santa Catarina', value: 'SC' },
          { label: 'São Paulo', value: 'SP' },
          { label: 'Sergipe', value: 'SE' },
          { label: 'Tocantins', value: 'TO' },
          ]}
          />)}

            rules={{ 
              required: 'O estado é obrigatório.',
            }}
 
            name = 'state'
            defaultValue = ''
           />
           {errors.estado && <Error>{errors.estado.message}</Error>}
       </View>

       <View>
           <Controller
            control = {control}
            render = {({field:{onBlur, onChange, value}})=> (
              <MyTextInput
                 placeholder='Cidade'
                 onBlur={onBlur}
                 onChangeText= {(value:any) => onChange(value)}
                 value={value}
              />
            )}

            rules={{ 
              required: 'A cidade é obrigatória.',
            }}
 
            name = 'city'
            defaultValue = ''
           />
           {errors.cidade && <Error>{errors.cidade.message}</Error>}
       </View>


       <Entrar onPress ={handleSubmit(onSubmit,onError)}>
           <Cadastrar>Cadastrar</Cadastrar>
       </Entrar>
  </Body>
);
}





