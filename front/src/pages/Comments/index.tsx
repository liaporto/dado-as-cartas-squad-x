import React, {useState, useEffect} from "react";
import { useNavigation } from "@react-navigation/native";

import { ScrollView, View, Text, TouchableOpacity } from "react-native";

import { AppView } from "../../global/styles";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import RatingStars from "../../components/RatingStars";
import Comment from "../../components/Comment";

import ModalPostComment from "../../components/ModalPostComment";

import {
  CommentPageHeader,
  CommentsScrollView,
  PageTitle,
  RatingsContainer,
  RatingsText,
  NumberOfComments,
  CommentsContainer,
} from "./styles";

import CommentService from '../../services/CommentService'
import ProductService from '../../services/ProductService'

interface Comment {
  id: number;
  description: string;
  rating: number;
  UserId: number;
  ProductId: number;
}

const Comments = (props:any) => {
  const navigation = useNavigation();

  const product_id = props.route.params.id;
  const product_rating = props.route.params.rating;
  const product_comments = props.route.params.productComments;
  const is_modal_visible = props.route.params.isModalVisible;

  const [comments, setComments] = useState<Comment[]>();
  const [rating, setRating] = useState(product_rating);
  const [isModalVisible, setIsModalVisbile] = useState(is_modal_visible || false);

  useEffect(()=> {
    CommentService.listComments().then(res => {
      if(res){
        const productComments = res.data.comments.filter(comment => comment.ProductId === product_id);
        setComments(productComments);
      }
    }).catch(err => console.log(err));

    if(!rating){
      ProductService.showProduct(product_id).then(res => {
        if(res){
          console.log(res.data);
          setRating(res.data.product.rating);
        }
      }).catch(err => console.log(err));
    }
    
  }, [isModalVisible])

  const changeModalVisibility = (visibility:boolean) => {
    setIsModalVisbile(visibility);
  }

  return (
    <AppView>
      <CommentsScrollView>
        <View>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon name="arrow-left" size={30} color="#11272C" />
          </TouchableOpacity>
        </View>
        {comments &&
          <>
            <CommentPageHeader>
              <PageTitle>Avaliações dos clientes</PageTitle>
              <RatingsContainer>
                <RatingStars iconSize={24} rating={rating}/>
                <RatingsText> ({rating})</RatingsText>
              </RatingsContainer>
              <NumberOfComments>{comments.length} avaliações totais</NumberOfComments>
            </CommentPageHeader>

            <CommentsContainer>
              {comments.map((comment) => <Comment key={comment.id} data={comment}/>)}
            </CommentsContainer>
          </>
        }

        <View style={{height: 100}}></View>
      </CommentsScrollView>
      <ModalPostComment isVisible={isModalVisible} user_id={1} product_id={product_id} changeModalVisibility={changeModalVisibility}/>
    </AppView>
  );
};

export default Comments;
