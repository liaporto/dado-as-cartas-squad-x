import styled from 'styled-components/native'

import {StyledText} from '../../global/styles'

export const CommentPageHeader = styled.View`
  height: 6%;
  margin: 5% 0 11%;
  justify-content: space-between;
`

export const CommentsScrollView = styled.ScrollView`
  padding: 60px 30px;
  background-color: #fff;
`

export const PageTitle = styled(StyledText)`
  font-size: 30px;
  font-family: SourceSansSemiBold;
`

export const RatingsContainer = styled.View`
  flex-direction: row;
`

export const RatingsText = styled(StyledText)`
  color: #77B21F;
  font-size: 16px;
  font-family: SourceSansSemiBold;
`

export const NumberOfComments = styled(StyledText)`
  font-size: 18px;
  font-family: SourceSansSemiBold;
`

export const CommentsContainer = styled.View`
  margin-top: 15%;
  flex: 1;
  justify-content: space-between;
`