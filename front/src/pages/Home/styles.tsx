import styled from "styled-components/native";

import { StyledText } from "../../global/styles";

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

export const HomeView = styled.View`
  flex: 1;
`

export const Body = styled.View`
  flex: 1;
  padding: ${hp('5%')}px ${wp('5%')}px;
`

export const ImageContainer = styled.View`
  width: 100%;
  padding: 5px;
`

export const TouchableBanner = styled.TouchableOpacity`
  height: 10%;
`

export const BannerImg = styled.Image`
  width: 100%;
  height: 100%;
`

export const ProductListsSection = styled.View`
  margin: 3% 0;
`

export const ProductListContainer = styled.View`
  margin: 10% 0;
`

export const ListTitle = styled(StyledText)`
  color: #fff;
  font-size: 20px;
  margin-bottom: 10px;
`