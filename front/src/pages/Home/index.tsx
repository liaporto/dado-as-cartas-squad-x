import React, {useState, useEffect} from "react";
import { Text, View, ScrollView, TouchableOpacity, Image } from "react-native";

import Header from "../../components/Header";
import ProductTile from "../../components/ProductTile";

import bannerImg from "../../../assets/banner_promocao.png";

import { ScrollAppView } from "../../global/styles";

import {
  HomeView,
  ProductListContainer,
  TouchableBanner,
  BannerImg,
  ListTitle,
  Body,
  ProductListsSection,
  ImageContainer,
} from "./styles";

import ProductService from "../../services/ProductService";
import UserService from '../../services/UserService';

interface Product {
  id: number;
  name: string;
  shipping: number;
  description: string;
  pricetag: number;
  brand: string;
  deliveryDate: Date;
  categories:string;
  rating: number;
  recommendedAge: string;
  playerNumber: string;
  mechanics: string;
  gameTime: string;
  createdAt: Date;
  updatedAt: Date;
  UserId: number;
  Image: {};
  supplier: string;
}

const Home = () => {

  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    
    ProductService.listProducts().then((res) => {
  
        if(res){
          setProducts(res.data.products);
        }
        else{
          console.log("erro");
        }
      })
  }, [])

  const getBrands = () => {
    const brands = products.map(product => {
      return product.brand;
    })

    return brands.filter((brand, index) => brands.indexOf(brand) === index);
  }

  const testImage = 'http://localhost:3333/uploads/1628043550116-categoria-placeholder.png'
  return (
    <HomeView>
      <ScrollAppView>
        <Header />

        <Body>
          <TouchableBanner>
            <BannerImg resizeMode="contain" source={bannerImg} />
          </TouchableBanner>
          <ProductListsSection>

            {/* Display por marca */}
            {getBrands().map((brand, index) => {
              return(
                <ProductListContainer key={index}>
                  <ListTitle>{brand}</ListTitle>
                  <ScrollView horizontal={true}>
                    {products && products.map(product => {
                      if(product.brand === brand){
                        return <ProductTile key={product.id} data={product}/>
                      }
                    })}
                  </ScrollView>
                </ProductListContainer>    
              )
            })}
          </ProductListsSection>

          {/* Dá um espaço entre o fim dos produtos e o fim do scroll*/}
          <View style={{height: 200}}></View>
        </Body>
      </ScrollAppView>
    </HomeView>
  );
};

export default Home;
