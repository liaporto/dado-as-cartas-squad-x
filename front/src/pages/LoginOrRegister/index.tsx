import React, {useState} from 'react'
import { useNavigation } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native'

import ModalLoginRequired from '../../components/ModalLoginRequired'

import logoImg from '../../../assets/Logo.png'

import {StyledLinkText, StyledPurpleButton, StyledButtonText} from '../../global/styles'
import {BackgroundImageDAC, ButtonContainer, LinkContainer, LogoImage} from './styles';

const LoginOrRegister = () => {

  const navigation = useNavigation();

  const [isPromptVisible, setIsPromptVisible] = useState(false);

  const changeModalVisibility = (visiblity:boolean) => {
    setIsPromptVisible(visiblity);
  }

  const handleNavigateToLogin = () => {
    navigation.navigate("Home");
  }

  return (
    <BackgroundImageDAC source={require('../../../assets/background-dac.jpg')} resizeMode="cover">

      {/* <ModalLoginRequired isVisible={isPromptVisible} changeModalVisibility={changeModalVisibility}/> */}

      <LogoImage source={logoImg} resizeMode="contain"/>

      <ButtonContainer>
        <StyledPurpleButton onPress={() => navigation.navigate("Login")}>
          <StyledButtonText>Entrar</StyledButtonText>
        </StyledPurpleButton>
        
        <StyledPurpleButton onPress={() => navigation.navigate("Register")}>
          <StyledButtonText>Cadastrar</StyledButtonText>
        </StyledPurpleButton>
      </ButtonContainer>
      
      <LinkContainer>
        <TouchableOpacity onPress={() => navigation.navigate("Tabs")}>
          <StyledLinkText>Continuar como visitante</StyledLinkText>
        </TouchableOpacity>
      </LinkContainer>
    </BackgroundImageDAC>
  )
}

export default LoginOrRegister