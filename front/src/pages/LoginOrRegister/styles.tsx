import styled from "styled-components/native";

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

export const BackgroundImageDAC = styled.ImageBackground`
  flex: 1;
  display: flex;
  height: ${hp('100%')};
  width: ${wp('100%')};
  padding: 15% 0;
  justify-content: space-between;
  align-items: center;
`

export const LogoImage = styled.Image`
  width: 50%;
  height: 30%;
`

export const ButtonContainer = styled.View`
  display: flex;
  width: 60%;
  height: 30%;
  align-items: center;
  justify-content: space-between;
`

export const LinkContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: space-around;
  height: 12%;
`