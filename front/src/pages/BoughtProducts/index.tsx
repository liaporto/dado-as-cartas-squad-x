import React from "react";
import { useNavigation } from "@react-navigation/native";

import { View, Text, ScrollView, TouchableOpacity, Image } from "react-native";

import { ScrollAppView } from "../../global/styles";

import ListedProductTile from "../../components/ListedProductTile";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import logoImage from "../../../assets/Logo.png";

import {Content, PageHeader, PageTitle, HeaderLogo, BoughtProductsContainer } from "./styles";

const BoughtProducts = () => {
  const navigation = useNavigation();

  return (
    <ScrollAppView>
      <Content>
        <PageHeader>
          <View>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Icon name="arrow-left" size={30} color="#ffffff" />
            </TouchableOpacity>
            <PageTitle>Comprados</PageTitle>
          </View>
            <HeaderLogo source={logoImage} resizeMode="contain"/>
        </PageHeader>
        <View>
          <ListedProductTile type="boughtProduct" />
          <ListedProductTile type="boughtProduct" />
          <ListedProductTile type="boughtProduct" />
          <ListedProductTile type="boughtProduct" />
          <ListedProductTile type="boughtProduct" />
          <ListedProductTile type="boughtProduct" />
          <ListedProductTile type="boughtProduct" />
        </View>
      </Content>
    </ScrollAppView>
  );
};

export default BoughtProducts;
