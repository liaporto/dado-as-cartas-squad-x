import React, { useState, useEffect, useContext } from "react";
import { useNavigation } from "@react-navigation/native";

import { AuthContext } from "../../contexts/auth";

import ProductService from '../../services/ProductService'
import UserService from '../../services/UserService'
import CommentService from "../../services/CommentService";
import OrderService from '../../services/OrderService'

import { AppView } from "../../global/styles";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import { Platform, View, ScrollView, Text, Image, TouchableOpacity } from "react-native";

import { DataTable } from "react-native-paper";
import Toast from 'react-native-root-toast';

import Header from "../../components/Header";
import RatingStars from "../../components/RatingStars";
import Comment from '../../components/Comment'
import ModalLoginRequired from '../../components/ModalLoginRequired'

import placeholderImg from "../../../assets/products/product-placeholder-img.jpg";

import {LinearGradient} from "expo-linear-gradient";

import {StyledText} from '../../global/styles'

import {
  ActionButtons,
  AddToCartFAB,
  ProductImageContainer,
  ProductImage,
  ProductInformation,
  MainInformation,
  ProductRatingsContainer,
  RatingsText,
  ProductName,
  ProductPrice,
  AddToCartCTA,
  AddToCartText,
  ShippingInformationContainer,
  ShippingInfoTitle,
  ShippingInfoText,
  ProductDetails,
  ProductDetailContainer,
  ProductDetailTitle,
  FeatureTitleContainer,
  FeatureText,
  CommentExampleSection,
  CommentExampleContainer,
  GoToCommentsCTA,
  GoToCommentsText,
} from "./styles";

interface ProductData{
  name: string;
  pricetag: number;
  rating: number;
  shipping: number;
  description: string;
  playerNumber: string;
  mechanics: string;
  gameTime: string;
  recommendedAge: string;
  categories: string;
}

const Product = (props:any) => {

  const Auth = useContext(AuthContext);

  const navigation = useNavigation();

  const product_id = props.route.params.id;


  const [isFavorited, setIsFavorited] = useState(false);

  const [isToastVisible, setIsToastVisible] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [productData, setProductData] = useState<ProductData>();
  const [shippingDate, setShippingDate] = useState<string>();

  const [sellerId, setSellerId] = useState(0);
  const [sellerUsername, setSellerUsername] = useState();

  const [productComments, setProductComments] = useState();
  const [exampleComment, setExampleComment] = useState();

  useEffect(()=> {

    // Configura os dados do produto
    ProductService.showProduct(product_id).then(res => {
      if(res){
        setProductData(res.data.product);
        setSellerId(res.data.product.sellerId);
      }
    }).catch(err => console.log(err));

    ProductService.listFavorite({UserId: 1}).then(res => {
      if(res){
        console.log(res.data);
        const userProductFavorites = res.data.listFavorites.filter(productUserFavorite => productUserFavorite.id === product_id );

        if(userProductFavorites.length > 0){
          setIsFavorited(true);
        }
      }
    }).catch(err => console.log(err));
    
    // Configura uma data de entrega padrão para 3 dias além da data atual
    setShippingDate(createShippingDate());

  }, [product_id])

  useEffect(()=> {

    // Puxa os comentários e configura um comentário exemplo
    CommentService.listComments().then(res => {
      if(res){
        const allProductComments = res.data.comments.filter(comment => comment.ProductId === product_id); 
        const exampleComment = allProductComments[0];
        
        setProductComments(allProductComments);
        setExampleComment(exampleComment);
      }
    }).catch(err => console.log(err));
  }, [productData])

  useEffect(()=> {
    UserService.showUser(sellerId).then(res => {
      if(res){
        setSellerUsername(res.data.user.username);
      }
    }).catch(err => console.log(err))
  }, [sellerId])

  const changeModalVisibility = (visibility:boolean) =>{
    setIsModalVisible(visibility);
  }

  const createShippingDate = () => {
    const date = new Date();
    date.setDate(date.getDate() + 3);
    return `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`;
  }

  const handleHeartIconPress = () => {
    const favoriteData = {
      ProductId: product_id,
      UserId: 1
    }

    if(isFavorited === false){
      ProductService.setFavorite(favoriteData).then(res =>{
        if(res){
          console.log(res.data)
        }
      }).catch(err => console.log(err));
    } else {
      ProductService.removeFavorite(favoriteData).then(res =>{
        if(res){
          console.log(res.data)
        }
      }).catch(err => console.log(err));
    }
    setIsFavorited(!isFavorited);
  };

  const handleGoToComments = (id:number) => {
    const rating = productData.rating;
    navigation.navigate("Comments", {id, productComments, rating});
  }

  const handleAddToCart = () => {
    
    // TODO: resolver isso aqui
    console.log(Auth.userId);

    if(Auth.signed){
      const orderData = {
        UserId: Auth.userId,
        ProductId: product_id,
        amount: 1
      }
      
      OrderService.createOrder(orderData).then(res=>{
        if(res){
          console.log(res.data);
          setIsToastVisible(true);
          console.log("Added to cart!");
        }
      }).catch(err => err);
  
  
      setTimeout(()=> {
        setIsToastVisible(false);
      }, 3000);

      navigation.navigate("Tabs", {
        screen: "Cart"
      });
    } else {
      setIsModalVisible(true);
    }

  }

  return (
    <AppView>
      <ScrollView>
        <Header />

        
        <ActionButtons>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon name="arrow-left" size={30} color="#fff" />
          </TouchableOpacity>

          <TouchableOpacity onPress={handleHeartIconPress}>
            <Icon
              size={36}
              name={isFavorited ? "heart" : "heart-outline"}
              color={isFavorited ? "#FB873F" : "#FFFFFF"}
            />
          </TouchableOpacity>
        </ActionButtons>

        {productData &&
        
          <>
          <ProductImageContainer>
            <ProductImage source={placeholderImg} resizeMode="contain" />
          </ProductImageContainer>

          
          <ProductInformation>
            
            <MainInformation>
              <ProductRatingsContainer>
                <RatingStars iconSize={24} rating={productData.rating}/>
                <RatingsText>({productComments && productComments.length} avaliações)</RatingsText>
              </ProductRatingsContainer>

              <ProductName>{productData.name}</ProductName>
              <ProductPrice>R${productData.pricetag.toFixed(2).replace(".", ",")}</ProductPrice>

              <AddToCartCTA onPress={handleAddToCart}>
                <AddToCartText>Adicionar ao carrinho</AddToCartText>
              </AddToCartCTA>
            </MainInformation>

            
            <ShippingInformationContainer>
              <View>
                <ShippingInfoTitle>Vendedor</ShippingInfoTitle>
                <ShippingInfoText>{sellerUsername}</ShippingInfoText>
              </View>

              <View>
                <ShippingInfoTitle>Frete</ShippingInfoTitle>
                <ShippingInfoText>R${productData.shipping.toFixed(2).replace(".", ",")}</ShippingInfoText>
              </View>

              <View>
                <ShippingInfoTitle>Previsão de entrega</ShippingInfoTitle>
                <ShippingInfoText>{shippingDate}</ShippingInfoText>
              </View>
            </ShippingInformationContainer>

            <ProductDetails>
              <ProductDetailContainer>
                <ProductDetailTitle>Descrição</ProductDetailTitle>
                <StyledText>
                  {productData.description}
                </StyledText>
              </ProductDetailContainer>

              <ProductDetailContainer>
                <ProductDetailTitle>Características</ProductDetailTitle>
                <DataTable>
                  <DataTable.Row style={{ paddingHorizontal: "0%" }}>
                    <DataTable.Cell>
                      <FeatureTitleContainer>
                        <Icon name="account-group" size={18} />
                        <FeatureText>Nº de jogadores</FeatureText>
                      </FeatureTitleContainer>
                    </DataTable.Cell>
                    <DataTable.Cell style={{ justifyContent: "flex-end" }}>
                      <FeatureText>{productData.playerNumber}</FeatureText>
                    </DataTable.Cell>
                  </DataTable.Row>
                  <DataTable.Row style={{ paddingHorizontal: "0%" }}>
                    <DataTable.Cell>
                      <FeatureTitleContainer>
                        <Icon name="tools" size={18} />
                        <FeatureText>Mecânica</FeatureText>
                      </FeatureTitleContainer>
                    </DataTable.Cell>
                    <DataTable.Cell style={{ justifyContent: "flex-end" }}>
                      <FeatureText>{productData.mechanics}</FeatureText>
                    </DataTable.Cell>
                  </DataTable.Row>
                  <DataTable.Row style={{ paddingHorizontal: "0%" }}>
                    <DataTable.Cell>
                      <FeatureTitleContainer>
                        <Icon name="timer-outline" size={18} />
                        <FeatureText>Tempo de partida</FeatureText>
                      </FeatureTitleContainer>
                    </DataTable.Cell>
                    <DataTable.Cell style={{ justifyContent: "flex-end" }}>
                      <FeatureText>{productData.gameTime}</FeatureText>
                    </DataTable.Cell>
                  </DataTable.Row>
                  <DataTable.Row style={{ paddingHorizontal: "0%" }}>
                    <DataTable.Cell>
                      <FeatureTitleContainer>
                        <Icon name="account-question" size={18} />
                        <FeatureText>Faixa etária</FeatureText>
                      </FeatureTitleContainer>
                    </DataTable.Cell>
                    <DataTable.Cell style={{ justifyContent: "flex-end" }}>
                      <FeatureText>{productData.recommendedAge}</FeatureText>
                    </DataTable.Cell>
                  </DataTable.Row>
                  <DataTable.Row style={{ paddingHorizontal: "0%" }}>
                    <DataTable.Cell>
                      <FeatureTitleContainer>
                        <Icon name="format-list-bulleted" size={18} />
                        <FeatureText>Categoria</FeatureText>
                      </FeatureTitleContainer>
                    </DataTable.Cell>
                    <DataTable.Cell style={{ justifyContent: "flex-end" }}>
                      <FeatureText>{productData.categories}</FeatureText>
                    </DataTable.Cell>
                  </DataTable.Row>
                </DataTable>
              </ProductDetailContainer>

              <CommentExampleSection>
                <ProductDetailTitle>Avaliações dos clientes</ProductDetailTitle>
                <CommentExampleContainer>
                  {exampleComment && <Comment data={exampleComment}/>}

                  <LinearGradient
                    // Background Linear Gradient
                    colors={['transparent', '#FAFDFF']}
                    start={{x: 0, y: 0.3}}
                    end={{x: 0, y: 0.8}}
                    style={{
                      position: 'absolute',
                      left: 0,
                      right: 0,
                      bottom: 0,
                      height: '100%',
                    }}
                  />

                  <GoToCommentsCTA onPress={() => handleGoToComments(product_id)}>
                    <GoToCommentsText>Ver todas</GoToCommentsText>
                  </GoToCommentsCTA>
                </CommentExampleContainer>
              </CommentExampleSection>

            </ProductDetails>

            
            <View style={{height:450}}></View>

          </ProductInformation>
          </>
        }

        </ScrollView>

        
        <AddToCartFAB onPress={handleAddToCart}>
          <Icon name="cart-plus" size={30} color="#fff" />
        </AddToCartFAB>
        
        <Toast
            visible={isToastVisible}
            position={-50}
            shadow={true}
            animation={true}
            hideOnPress={true}
            backgroundColor="#FFF"
            textColor="#102428"
            opacity={1}
          >Item adicionado ao carrinho!</Toast>

        <ModalLoginRequired isVisible={isModalVisible} changeModalVisibility={changeModalVisibility} />
      </AppView>
  );
};

export default Product;
