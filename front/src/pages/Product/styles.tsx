import styled from 'styled-components/native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

import { StyledText } from '../../global/styles'

import Comment from '../../components/Comment'

export const ActionButtons = styled.View`
  display: flex;
  width: 100%;
  padding: 5% 8% 5% 5%;
  flex-direction: row;
  justify-content: space-between;
`

export const AddToCartFAB = styled.TouchableOpacity`
  position: absolute;
  bottom: 3%;
  right: 8%;
  width: 70;
  height: 70;
  padding: 4%;

  justify-content: center;
  align-items: center;

  background-color: #77B21F;
  border-radius: 100px;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.25);
`

export const ProductImageContainer = styled.View`
  align-items: center;
  width: 100%;
  height: 20%;
`

export const ProductImage = styled.Image`
  height: 100%;
  width: 50%;
`

export const ProductInformation = styled.View`
  flex: 1;
  padding: 20px 30px;
  align-items: center;

  background-color: #fff;

  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
`

export const MainInformation = styled.View`
  display: flex;
  height: 13%;
  width: 100%;
  margin: 30px 0;
  justify-content: space-between;
`

export const ProductRatingsContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

export const RatingsText = styled(StyledText)`
  font-size: 16px;
  color: #77B21F;
  font-family: SourceSansSemiBold;
`

export const ProductName = styled(StyledText)`
  text-align: center;
  font-size: 26px;
  font-family: SourceSansSemiBold;
`

export const ProductPrice = styled.Text`
  text-align: center;
  
  color: #77B21F;
  font-size: 26px;
  font-family: SourceSansBold;
`

export const AddToCartCTA = styled.TouchableOpacity`
  display: flex;
  width: 100%;
  height: 22%;
  align-items: center;
  justify-content: center;

  border-radius: 5px;
  background-color: #77B21F;
  `

export const AddToCartText = styled(StyledText)`
  font-size: 16px;
  color: #fff;
  font-family: SourceSansSemiBold;
  `

export const ShippingInformationContainer = styled.View`
  display: flex;
  height: 13%;
  width: 100%;
  margin: 30px 0;
  justify-content: space-between;
`

export const ShippingInfoTitle = styled(StyledText)`
  font-size: 18px;
  font-family: SourceSansSemiBold;
`

export const ShippingInfoText = styled(StyledText)`
  font-size: 18px;
`

export const ProductDetails = styled.View`
  height: 60%;
`

export const ProductDetailContainer = styled.View`
  margin: 30px 0;
`

export const ProductDetailTitle = styled(StyledText)`
  margin-bottom: 15px;
  font-size: 26px;
  font-family: SourceSansSemiBold;
`

export const FeatureTitleContainer = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
`

export const FeatureText = styled.Text`
  font-family: SourceSans;
  margin-left: 5px;
`

export const CommentExampleSection = styled.View`
  height: 30%;
  margin: 30px 0;
`

export const CommentExampleContainer = styled.View`
  height: 70%;
`

export const GoToCommentsCTA = styled.TouchableOpacity`
  width: 100%;
  height: 25%;
  margin-top: 10%;
  align-items: center;
  justify-content: center;
  
  background-color: #fff;

  border-radius: 5px;
  border-width: 1px;
  border-style: solid;
  border-color: #102428;
`

export const GoToCommentsText = styled(StyledText)`
  font-size: 16px;
  font-family: SourceSansSemiBold;
`