import styled from 'styled-components/native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

import { StyledText, AppView, StyledOrangeButton, StyledGreenButton, StyledButtonText } from '../../global/styles'

export const MainView = styled.View`
  height: 100%;
`

export const Body = styled.View`
flex: 1;
  width: 100%;
  padding: 5% 0 0;
`

export const CartTitle = styled(StyledText)`
  margin: 5%;

  color: #fff;
  font-size: 30px;
  font-family: SourceSansSemiBold;
`

export const FilledCartSection = styled.View`
  height: 100%;
`

export const CartItemsContainer = styled.ScrollView`
  height: 40%;
  padding: 0 5%;
`

export const CartInfo = styled.View`
  height: 50%;
  padding: 30px 30px;
  align-items: center;

  background-color: #fff;

  border-top-left-radius: 30px;
  border-top-right-radius: 30px;

  border-top-width: 1px;
  border-color: #000;
`

export const PriceInfo = styled.View`
  width: 100%;

  margin: 2% 0;

  flex-direction: row;
  justify-content: space-between;
`

export const PriceInfoText = styled(StyledText)`
  font-size: 20px;
  font-family: SourceSansSemiBold;
`

export const Divider = styled.View`
  width: 100%;
  height: 1px;

  margin: 1% 0;

  border-top-width: 1px;
  border-top-color: #C4C4C4;
  border-style: solid;
`

export const TotalPriceInfo = styled.View`
  width: 100%;

  margin: 5% 0;

  flex-direction: row;
  justify-content: space-between;
`

export const TotalPriceInfoText = styled(StyledText)`
  font-size: 24px;
  font-family: SourceSansBold;
`

export const TotalPriceInfoValue = styled(StyledText)`
  color: #77B21F;
  font-size: 24px;
  font-family: SourceSansBold;
`

export const StyledFinishPaymentButton = styled(StyledGreenButton)`
  height: 15%;
`

export const EmptyCartSection = styled.View`
  width: 100%;
  padding: 0 10%;
  justify-content: center;
  align-items: center;
`

export const EmptyCartText = styled(StyledText)`
  color: #fb873f;
  font-size: 18px;
`

export const EmptyCartGraphic = styled.View`
  width: ${wp('50%')};
  height: ${wp('50%')};
  margin: 10% 0;

  align-items: center;
  justify-content: center;

  border-style: solid;
  border-width: 1px;
  border-color: #FB873F;
  border-radius: 100px;
`

export const StyledAddItemsButton = styled(StyledOrangeButton)`
  height: 10%;
`

export const StyledAddItemsText = styled(StyledButtonText)`
  font-size: 16px;
`