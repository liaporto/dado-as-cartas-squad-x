import React, {useState, useEffect, useContext} from 'react'
import { AuthContext } from "../../contexts/auth";
import {useNavigation} from '@react-navigation/native'
import {View, Text, ScrollView, TouchableOpacity, Alert} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import ModalLoginRequired from '../../components/ModalLoginRequired'
import CartProductTile from '../../components/CartProductTile'

import Toast from 'react-native-root-toast';

import OrderService from '../../services/OrderService'

import {AppView, StyledGreenButton, StyledButtonText} from '../../global/styles'

import {MainView, Body, CartTitle, FilledCartSection, CartItemsContainer, CartInfo, PriceInfo, PriceInfoText, TotalPriceInfo, TotalPriceInfoText, TotalPriceInfoValue, Divider, StyledFinishPaymentButton, EmptyCartSection, EmptyCartText, EmptyCartGraphic, StyledAddItemsButton, StyledAddItemsText} from './styles'

const Cart = () => {

  const Auth = useContext(AuthContext);

  const navigation = useNavigation();

  const [isToastVisible, setIsToastVisible] = useState(false);

  //PROVISÓRIO
  const [isLogged, setIsLogged] = useState(true);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [hasItems, setHasItems] = useState(false);

  useEffect(()=> {
    if(Auth.signed){
      console.log("signed");
    } else {
      console.log("unsigned");
    }
  }, [])

  const changeModalVisibility = (visibility:boolean) =>{
    setIsModalVisible(visibility);
  }

  const handleFinishPurchase = () => {
    if(Auth.signed){
      setIsToastVisible(true);
      setHasItems(false);

      setTimeout(()=> {
        setIsToastVisible(false);
      }, 3000);
    } else {
      setIsModalVisible(true);
    }
  }

  return (
    <AppView>
      <MainView>
        <Body>
          <CartTitle>Carrinho</CartTitle>
        

          {hasItems ?
            <FilledCartSection>
              
              <CartItemsContainer>
                <CartProductTile/>
                <CartProductTile/>
                <CartProductTile/>
                <CartProductTile/>
                <CartProductTile/>
              </CartItemsContainer>


              <CartInfo>
                <PriceInfo>
                  <PriceInfoText>Subtotal</PriceInfoText>
                  <PriceInfoText>R$500,00</PriceInfoText>
                </PriceInfo>
                <PriceInfo>
                  <PriceInfoText>Frete</PriceInfoText>
                  <PriceInfoText>R$42,00</PriceInfoText>
                </PriceInfo>
                <Divider></Divider>
                <TotalPriceInfo>
                  <TotalPriceInfoText>Total</TotalPriceInfoText>
                  <TotalPriceInfoValue>R$542,00</TotalPriceInfoValue>
                </TotalPriceInfo>


                <StyledFinishPaymentButton onPress={handleFinishPurchase}>
                  <StyledButtonText>Concluir compra</StyledButtonText>
                </StyledFinishPaymentButton>
                  
              </CartInfo>
            </FilledCartSection>
            :
            <EmptyCartSection>
              <EmptyCartText>Seu carrinho está vazio</EmptyCartText>
              <EmptyCartGraphic>
                <Icon name="cart-outline" size={80} color="#FB873F"/>
              </EmptyCartGraphic>
              <StyledAddItemsButton onPress={()=> navigation.navigate("Home")}>
                <StyledAddItemsText>Adicione itens</StyledAddItemsText>
              </StyledAddItemsButton>
            </EmptyCartSection>
          }
        </Body>
      </MainView>

      <Toast
          visible={isToastVisible}
          position={-50}
          shadow={true}
          animation={true}
          hideOnPress={true}
          backgroundColor="#FFF"
          textColor="#102428"
          opacity={1}
        >Compra efetuada!</Toast>

      <ModalLoginRequired isVisible={isModalVisible} changeModalVisibility={changeModalVisibility}/>
    </AppView>
  )
}

export default Cart
