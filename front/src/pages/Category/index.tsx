import React, {useState, useEffect} from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, ScrollView, Text, FlatList } from 'react-native';
import { SafeAreaView, SafeAreaProvider, } from 'react-native-safe-area-context';

import Header from '../../components/Header';
import ProductTile from '../../components/ProductTile';

import { ScrollAppView } from "../../global/styles";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import {
    Arrow,
    ProductListContainer,
    CategoryTitle,
    Row,
    Body,
  } from "./style";

import ProductService from '../../services/ProductService'

const Category = (props:any) => {
    const navigation = useNavigation();
    
    const category_name = props.route.params.name;

    const [products, setProducts] = useState();

    useEffect(()=> {
        ProductService.listProducts().then(res => {
            if(res){
                const allProducts = res.data.products;
                const categoryProducts = allProducts.filter(product => {
                    return product.categories.includes(category_name);
                });
                console.log(categoryProducts[0]);
                setProducts(categoryProducts);
            }
        })
    }, [])

    return ( 

        <View>
        <Header/>
    
        
        <Body>
        <Arrow onPress={() => navigation.goBack()}> 
            <Icon name="arrow-left" color="#FFFFFF" size={28}/>
        </Arrow>
        <CategoryTitle>{category_name}</CategoryTitle>
        <Row>
            <ProductListContainer>
                {products && products.map(product => {
                    return(<ProductTile key={product.id} data={product}/>)
                })}
            </ProductListContainer>
        </Row>
        <View style={{height: 200}}></View>
        </Body>
       </View>

    );
};

export default Category;