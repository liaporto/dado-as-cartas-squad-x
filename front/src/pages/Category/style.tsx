import styled from 'styled-components/native';

import { StyledText } from "../../global/styles";

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

export const Body = styled.ScrollView`
  background-color: #102428;
`

export const Arrow = styled.TouchableOpacity`
padding-top: 15%;
padding-left: 5%;
`

export const ProductListContainer = styled.View`
  width: 100%;
  margin: 5% 0;
  
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
`

export const CategoryTitle = styled(StyledText)`
  color: #FFFFFF;
  font-size: 30px;
  margin: 5%;
  padding-top:10px;
`

export const Row = styled.View`
  flex-direction: row;
  height:100%;
  width: 100%;
`
