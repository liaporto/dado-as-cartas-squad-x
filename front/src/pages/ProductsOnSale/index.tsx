import React from "react";
import { useNavigation } from "@react-navigation/native";

import { View, Text, ScrollView, TouchableOpacity, Image } from "react-native";

import { AppView, ScrollAppView } from "../../global/styles";

import ListedProductTile from "../../components/ListedProductTile";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import logoImage from "../../../assets/Logo.png";

import {AddProductFAB, Content, PageHeader, PageTitle, HeaderLogo, BoughtProductsContainer } from "./styles";

const ProductsOnSale = () => {
  const navigation = useNavigation();

  const handleAddProduct = () =>{
    // TODO: fazer a lógica
    console.log("add product")
  }

  return (
    <AppView>
      <ScrollView>
        <Content>
          <PageHeader>
            <View>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <Icon name="arrow-left" size={30} color="#ffffff" />
              </TouchableOpacity>
              <PageTitle>Publicados</PageTitle>
            </View>
              <HeaderLogo source={logoImage} resizeMode="contain"/>
          </PageHeader>
          <View>
            <ListedProductTile type="productOnSale" />
            <ListedProductTile type="productOnSale" />
            <ListedProductTile type="productOnSale" />
            <ListedProductTile type="productOnSale" />
            <ListedProductTile type="productOnSale" />
            <ListedProductTile type="productOnSale" />
            <ListedProductTile type="productOnSale" />
          </View>
        </Content>
      </ScrollView>
      <AddProductFAB onPress={handleAddProduct}>
        <Icon name="plus" size={30} color="#fff" />
      </AddProductFAB>
    </AppView>
  );
};

export default ProductsOnSale;
