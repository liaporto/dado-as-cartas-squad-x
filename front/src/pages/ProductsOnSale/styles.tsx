import styled from 'styled-components/native'

import { StyledText } from '../../global/styles'

export const AddProductFAB = styled.TouchableOpacity`
  position: absolute;
  bottom: 3%;
  right: 8%;
  width: 70;
  height: 70;
  padding: 4%;

  justify-content: center;
  align-items: center;

  background-color: #FB873F;
  border-radius: 100px;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.25);
`

export const Content = styled.View`
  padding: 15% 5%;
`

export const PageHeader = styled.View`
  width: 100%;
  margin-bottom: 10%;
  flex-direction: row;
  justify-content: space-between;
`

export const PageTitle = styled(StyledText)`
  margin-top: 10px;
  color: #fff;
  font-family: SourceSansSemiBold;
  font-size: 30px;
`

export const HeaderLogo = styled.Image`
  width: 20%;
  height: 100%;
`