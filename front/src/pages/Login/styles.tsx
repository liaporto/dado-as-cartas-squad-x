import styled from 'styled-components/native';

export const Body = styled.View`
flex: 1;
background-color: #102024;

`

export const Arrow = styled.TouchableOpacity`
padding-top: 15%;
padding-left: 8%;
`


export const Title = styled.Text `
font-family: SourceSans ;
font-weight: normal;
font-size: 36px;
color: #77B21F;
align-self: center;
`

export const Subtitle = styled.Text `
font-family: SourceSans ;
font-weight: normal;
font-size: 18px;
color: #FAFDFF;
margin-left: 10%; 
`


export const MyImage = styled.Image `
    width: 140px;
    height: 148px;
    margin: 5%;
    align-self: center;
`

export const MyTextInput = styled.TextInput `
    font-family: SourceSans ;
    font-weight: 400;
    font-size: 10px; 
    padding-left: 5%;
    margin: 2%;
    align-self: center;
    background-color: #FAFDFF;
    width: 321px;
    height: 50px;
    border-radius: 5px;
`

export const Error = styled.Text `
    align-self: center;
    color: red;
`

export const Entrar = styled.TouchableOpacity`
    align-self: center;
    background-color: #77B21F;
    height: 40px;
    width: 227px;
    border-radius: 5px;
    margin-bottom: 10%;
    margin-top: 15%;
`

export const Cadastrar = styled.Text`
    text-align: center;
    font-family: SourceSans ;
    font-weight: bold;
    font-size: 16px; 
    color: #FAFDFF;
    margin-top: 10px;
`


export const Still = styled.TouchableOpacity`
    align-self: center;
`

export const Stilldont = styled.Text`
    text-align: center;
    font-family: SourceSans ;
    font-weight: 400;
    font-size: 16px; 
    color: #FAFDFF;
    /* margin-top: 10px; */
    text-decoration-line:underline;
`

