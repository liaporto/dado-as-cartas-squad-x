import React, { useContext, useState } from 'react';
import {View, TextInput, Text, TouchableOpacity, Image,  } from 'react-native';
import { useForm, Controller } from 'react-hook-form';
import {Body,Title, Subtitle, MyImage , MyTextInput, 
        Error , Entrar, Cadastrar, Stilldont, Still, Arrow 
} from './styles';
import logo from '../../../assets/logo/logo.png';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';
import api from '../../services/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { AppView } from '../../global/styles';
import { AuthContext } from '../../contexts/auth';

interface EditData{
  id: number;
  username: string;
  email: string;
  password: string;
  city: string;
}

export default function Login () {
    const Auth = useContext(AuthContext);
    const navigation = useNavigation();
    const { control, handleSubmit, formState:{errors} } = useForm({mode: 'onTouched'});
    
    const [userDetails, setUserDetails] = useState<EditData>();

    const [userId, setUserId] = useState<string>();

    const onSubmit = (data: FormData) => { 
      console.log(data)
      api.post('/login', data).then(response => {
        console.log(response.data)
        const token = response.data.token;
        AsyncStorage.setItem('token', token);
        Auth.setToken('Bearer ' + token);

        alert('Login feito com sucesso!');
        navigation.navigate('Tabs');

        api.get('/private/getDetails', {headers: {Authorization:Auth.token}}).then(res =>{
          setUserId(res.data.user.id);
          setUserDetails(res.data.user);
          if(userId){
            AsyncStorage.setItem('userId', userId);
          }
        }).catch(err => console.log(err));

        alert('Login feito com sucesso!');
        navigation.navigate('Tabs');
    } ).catch
    (error => ('Login não pode ser concluído.'))
  };

    interface FormData{ 
        email: string;
        password: string;
    }
    
    return (
      <Body>

        <Arrow onPress={()=> navigation.navigate("LoginOrRegister")}>
          <Icon name="arrow-left" color="#FFFFFF" size={28}/>
        </Arrow>

        <MyImage source={logo} resizeMode='contain'/>
        
        <Title>Entrar</Title>

        <View>

          <Subtitle>E-mail</Subtitle>

               <Controller
                control = {control}
                render = {({field:{onBlur, onChange, value}})=> (
                  <MyTextInput
                     placeholder='E-mail'
                     onBlur={onBlur}
                     onChangeText= {(value:any) => onChange(value)}
                     value={value}
                  />
                )}
                rules={{ 
                  required: 'O e-mail é obrigatório.',
                   pattern: {
                       value: /^\S+@\S+$/i,
                     message: 'Formato de e-mail inválido.'
                   },
              }}
     
                name = 'email'
                defaultValue = ''
               />
                {errors.email && <Error>{errors.email.message}</Error>}

        </View>

        <View>
           <Subtitle>Senha</Subtitle>
               <Controller
                control = {control}
                render = {({field:{onBlur, onChange, value}})=> (
                  <MyTextInput
                     placeholder='Senha'
                     secureTextEntry
                     onBlur={onBlur}
                     onChangeText= {(value:any) => onChange(value)}
                     value={value}
                  />
                )}
                name = 'password'
                defaultValue = ''
               />
     
        </View>


        <Entrar onPress ={handleSubmit(onSubmit)}>
        <Cadastrar>Entrar</Cadastrar>
        </Entrar>

    
        <Still onPress={()=> navigation.navigate("Register")} >
        <Stilldont>Ainda não tenho cadastro</Stilldont>
        </Still>
  

      </Body>
    );
} 
