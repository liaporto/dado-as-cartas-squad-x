import React, {useState, useEffect, useContext} from 'react';
import {AuthContext} from '../../contexts/auth';

import AsyncStorage from '@react-native-async-storage/async-storage';

import ModalLoginRequired from '../../components/ModalLoginRequired'

import {
  StyleSheet,
  View,
  Image,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  
} from 'react-native';

import { Avatar,Title,Caption,Text,TouchableRipple } from 'react-native-paper';
// import { Icon } from 'react-native-paper/lib/typescript/components/Avatar/Avatar';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Tabs from '../../components/Tabs';
import { useNavigation } from '@react-navigation/native';

import { AppView, StyledOrangeButton, StyledButtonText } from '../../global/styles';

import {
   UserInfoSection,
   OptionsBox,
   Options,
   LogOut,
   LogOutText,
   MyTitle,
   MyCity,
   Box,
} from "./styles"

const Profile = () => {
    const Auth = useContext(AuthContext);

    const navigation = useNavigation ();

    const [isModalVisible, setIsModalVisible] = useState(false);

    useEffect(()=>{
        console.log(Auth.signed);
        if(!Auth.signed){
            setIsModalVisible(true);
        }
    }, [])

    const handleLogOut = () => {
        AsyncStorage.removeItem("token");
        Auth.setToken("");
        Auth.setUser("");
        navigation.navigate("LoginOrRegister");
    }

    const changeModalVisibility = (visibility:boolean) =>{
        setIsModalVisible(visibility);
      }

    return (
        <SafeAreaView style={{flex:1, backgroundColor:"#102024"}}>
       
        <UserInfoSection>
          {Auth.signed ?
          
            <View>
                <View style={{flexDirection:"row"}}>

                <Avatar.Image
                size={75}
                source={require('../../../assets/profile/perfil-placeholder.png')}
                />

                <View style={{marginLeft:20, marginTop:10}}>
                    <MyTitle>Mari</MyTitle>
                    <MyCity>Cidade</MyCity>

                </View>
            </View>
            
            <Box>
                <OptionsBox onPress={()=> navigation.navigate("EditProfile")}>
                    <Options> <Icon name="pencil" color="#FFFFFF" size={20}/> Editar Perfil</Options>
                </OptionsBox>

                <OptionsBox onPress={()=> navigation.navigate("BoughtProducts")}>
                    <Options><Icon name="shopping" color="#FFFFFF" size={20}/>Minhas Compras</Options>
                </OptionsBox>

                <OptionsBox onPress={()=> navigation.navigate("Favorites")}>
                    <Options><Icon name="heart" color="#FFFFFF" size={20}/>Favoritos</Options>
                </OptionsBox>

                <OptionsBox onPress={()=> navigation.navigate("ProductsOnSale")}>
                    <Options><Icon name="tag" color="#FFFFFF" size={20}/>Meus jogos à venda</Options>
                </OptionsBox>

                <OptionsBox>
                    <Options><Icon name="tag-plus" color="#FFFFFF" size={20}/>Vender um jogo</Options>
                </OptionsBox>
            </Box>
                <LogOut onPress={()=> {handleLogOut()}}><LogOutText>Sair</LogOutText></LogOut>
                
            </View>

            :
            <View style={{justifyContent: 'center', height: '80%'}}>
                <StyledOrangeButton style={{height: '20%', marginBottom: '10%'}} onPress={()=> navigation.navigate("Login")}>
                    <StyledButtonText>Entrar</StyledButtonText>
                </StyledOrangeButton>
                <StyledOrangeButton style={{height: '20%'}} onPress={()=> navigation.navigate("Register")}>
                    <StyledButtonText>Cadastrar</StyledButtonText>
                </StyledOrangeButton>
                <ModalLoginRequired isVisible={isModalVisible} changeModalVisibility={changeModalVisibility} />
            </View>
        } 
        </UserInfoSection>

        
        </SafeAreaView>
       

    ) 

    }
  
export default Profile;
