import styled from 'styled-components/native';



export const UserInfoSection = styled.View`
margin-bottom: 25px;
margin-top:25px;
padding: 10%;
`

export const MyTitle = styled.Text`
font-size: 20px;
font-weight:normal;
color:#FFFFFF
`

export const MyCity = styled.Text`
font-size: 16px;
font-weight:normal;
color:#FFFFFF
`

export const Box = styled.View`
margin-top: 50px;
margin-bottom:50px;
`

export const OptionsBox = styled.TouchableOpacity`
    margin:5%;
    border-radius: 5px;
`

export const Options = styled.Text`
    font-weight: normal;
    font-size: 18px; 
    color: #FAFDFF;
    margin-top: 10px;
    
`
export const LogOut = styled.TouchableOpacity`
    background-color:#FB873F; 
    height: 40px;
    width: 227px;
    border-radius: 5px;
    align-self: center;
    margin-top: 10px;
`
export const LogOutText = styled.Text`
    text-align: center;
    font-weight: bold;
    font-size: 16px; 
    color: #102428;
    margin-top: 10px;
`