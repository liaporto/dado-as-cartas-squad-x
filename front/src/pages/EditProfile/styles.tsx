import styled from 'styled-components/native';



export const Body = styled.View`
background-color: #102428;
`
export const Arrow = styled.TouchableOpacity`
padding-top: 5%;
padding-left: 5%;
`
export const Title = styled.Text`
    font-size: 24px;
    color: #FFFFFF;
    text-align:center;
    font-weight:bold;
    margin-top:10%;
`
export const SubTitle = styled.Text`
    font-size: 18px;
    color: #FFFFFF;
    text-align:center;
    font-weight:300;
    padding:5%;
`
export const BoldText = styled.Text`
    font-size: 18px;
    color: #FFFFFF;
    text-align:center;
    font-weight:bold;
`
export const Optional = styled.Text`
    font-size: 18px;
    color: #FFFFFF;
    text-align:center;
    font-weight:normal;
`

export const Button = styled.TouchableOpacity`
    align-self: center;
    background-color: #77B21F;
    width: 40px;
    height: 40px;
    border-radius: 30px;
    margin-bottom: 2%;
    margin-top: 5%;
`
export const MyTextInput = styled.TextInput `
    font-family: SourceSans;
    font-weight: normal;
    font-size: 10px;
    padding-left: 5%;
    margin: 3%;
    align-self: center;
    background-color: #FAFDFF;
    width: 321px;
    height: 30px;
    border-radius: 5px;
`
export const MyIcon = styled.Text`
  padding: 7px;
`


export const Send = styled.TouchableOpacity`
    align-self: center;
    background-color: #77B21F;
    width: 154px;
    height: 37px;
    border-radius: 5px;
    margin-bottom: 15%;
    margin-top: 5%;
`
export const SendText = styled.Text`
    font-size: 18px;
    color:#162E33;
    text-align:center;
    font-weight:normal;
    padding:4%;
`