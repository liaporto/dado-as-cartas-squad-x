import React, { useEffect, useState } from "react";
// import {View, TextInput, Text, TouchableOpacity, Image, Dimensions, ScrollView, } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Body,Arrow,Title,SubTitle,BoldText,Optional,Button,MyTextInput, MyIcon, Send, SendText} from "./styles";
import { SafeAreaView } from "react-native-safe-area-context";
import {Camera} from "expo-camera";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { useNavigation } from "@react-navigation/native";

const EditProfile = () => {
const Tab = createBottomTabNavigator();
const navigation = useNavigation ()

    function handleSubmit(onSubmit: any): ((event: import("react-native").GestureResponderEvent) => void) | undefined {
        // throw new Error("Function not implemented.");
    }
    const onSubmit = (data:FormData) => {
        console.log(data);
    }
    const [hasPermission, setHasPermission] = useState (null);
    const [picture, setPicture] = useState([]);
    const [type, setType] = useState (Camera.Constants.Type.front);
    const [camera, setCamera] = useState ();

    useEffect (() => {
        (async () => { 
            const { status } = await Camera.requestCameraPermissionsAsync();
            setHasPermission (status === "granted"); 
        }) ();
    },[]);
    
return (
    <SafeAreaView style={{flex:1, backgroundColor:"#102024"}}>
    

     <Arrow onPress={() => navigation.goBack()}> 
      <Icon name="arrow-left" color="#FFFFFF" size={28}/>
      </Arrow>
     <Title>Preencha seu perfil</Title>
     <SubTitle>O perfil permite que você compartilhe um pouco sobre você com 
               outros clientes da DAC quando publicar avaliações e produtos.
    </SubTitle>
    <BoldText>Fazer upload de uma foto de perfil</BoldText>
    <Optional>(Opcional)</Optional>

   
    <Camera
     style={{ 
        marginTop:30,
        width: 98,
        minHeight: 98,
        alignSelf: "center",
    }}
     type={type}
     ref={(r) => {
         setCamera(r);
    }}
    >
    </Camera>
   

    <Button
             onPress={()=>
             {if(camera) {
             camera
             .takePictureAsync({ skipProcessing: true, base64: true, exif: true})
             .then((data:any) => {
                 setPicture((pics) => [...pics,data.uri]);
    //data.base64
             }).catch((err:any) => console.log(err),
             )}}
            } 
    >
       <MyIcon><Icon name="camera-plus" color="#FFFFFF" size={23}/></MyIcon> 
    </Button>
   
    <MyTextInput placeholder='Nome público'></MyTextInput>
    <MyTextInput placeholder='Cidade'></MyTextInput>
    <Send onPress ={handleSubmit(onSubmit)}><SendText>Enviar</SendText></Send>
    



   
    </SafeAreaView>
   
    
);
};


export default EditProfile;

