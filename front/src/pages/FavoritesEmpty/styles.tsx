import styled from 'styled-components/native';

import { StyledText } from "../../global/styles";

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";



export const Body = styled.View`
  padding-left: 4%;
`

export const ImageContainer = styled.View`
  width: 100%;
  padding: 5px;
`

export const TouchableBanner = styled.TouchableOpacity`
  height: 10%;
`

export const BannerImg = styled.Image`
  width: 100%;
  height: 100%;
`

export const ProductListsSection = styled.View`
  margin: 3% 0;
  background-color: #102428;
`

export const ProductListContainer = styled.View`
  margin: 10% 0;
`

export const FavoritesTitle = styled(StyledText)`
  color: #FFFFFF;
  font-size: 30px;
  margin: 10px;
  padding-top:10px;
`


export const FavoritesSubTitle = styled(StyledText)`
  color: #6E4CB2;
  font-size: 18px;
  text-align:center;
  margin-top:20%;
`

export const MyImage = styled.Image `
   
    margin: auto;
    margin-top: 10%;
    align-content: center;
    
`

export const Button = styled.TouchableOpacity`
    align-self: center;
    background-color: #6E4CB2;
    height: 40px;
    width: 261px;
    border-radius: 5px;
    margin-bottom: 15%;
    margin-top: 15%;
`


export const ButtonText = styled.Text`
    text-align: center;
    font-family: SourceSans ;
    font-weight: bold;
    font-size: 16px; 
    color: #FAFDFF;
    margin-top: 10px;
`