import React from 'react';
import { View, ScrollView, Text, FlatList, Image} from 'react-native';
import { SafeAreaView, SafeAreaProvider, } from 'react-native-safe-area-context';


import heart from '../../../assets/heart/heartIcon.png'
import Header from '../../components/Header';
import ProductTile from '../../components/ProductTile';

import { ScrollAppView } from "../../global/styles";

import {
    ProductListContainer,
    FavoritesTitle,
    Body,
    FavoritesSubTitle,
    MyImage,
    Button,
    ButtonText,
  } from "./styles";

  import { NavigationContainer } from '@react-navigation/native';
//   import { navigationRef } from './RootNavigation';
  


const FavoritesEmpty = () => {
    return ( 

        <View style={{flex:1, backgroundColor:"#102024"}}>
        <Header/>
    
        <Body>
        <FavoritesTitle>Favoritos</FavoritesTitle>
        <FavoritesSubTitle>Sua lista está vazia</FavoritesSubTitle>
        <MyImage source={heart} resizeMode='contain'/>
        <Button>
        <ButtonText>Adicione Itens</ButtonText>
        </Button>
        </Body>
       </View>

    );
}; 

export default FavoritesEmpty;









