import React from 'react';
import { View, ScrollView, Text, FlatList } from 'react-native';
import { SafeAreaView, SafeAreaProvider, } from 'react-native-safe-area-context';

import Header from '../../components/Header';
import ProductTile from '../../components/ProductTile';

import { ScrollAppView } from "../../global/styles";

import {
    ProductListContainer,
    Row,
    Body,
    Column,
  } from "./styles";

  import { NavigationContainer } from '@react-navigation/native';
  import { navigationRef } from './RootNavigation';
  


const SearchResult = () => {
    return ( 

        <View>
        <Header/>
    
        
        <Body>
        <Row>
            <Column>
                <ProductListContainer>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                </ProductListContainer>
            </Column>
           
            <Column>
                <ProductListContainer>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                </ProductListContainer>
            </Column>
        </Row>
        <View style={{height: 200}}></View>
        {/* <Tabs/> */}
        </Body>
       </View>

    );
};

export default SearchResult;









