import styled from 'styled-components/native';

import { StyledText } from "../../global/styles";

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";



export const Body = styled.ScrollView`
  background-color: #102428;
  padding-left: 4%;
`

export const ImageContainer = styled.View`
  width: 100%;
  padding: 5px;
`

export const TouchableBanner = styled.TouchableOpacity`
  height: 10%;
`

export const BannerImg = styled.Image`
  width: 100%;
  height: 100%;
`

export const ProductListsSection = styled.View`
  margin: 3% 0;
  background-color: #102428;
`

export const ProductListContainer = styled.View`
  margin: 10% 0;
`

export const CategoryTitle = styled(StyledText)`
  color: #FFFFFF;
  font-size: 30px;
  margin: 10px;
  padding-top:10px;
`

export const Column = styled.View` 
  flex: .5;
`

export const Row = styled.View`
  flex-direction: row;
  height:100%;
`
