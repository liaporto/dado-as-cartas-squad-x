import React, { useContext, useEffect, useState } from 'react';
import { View, ScrollView, Text, FlatList, Alert,  } from 'react-native';
import { SafeAreaView, SafeAreaProvider, } from 'react-native-safe-area-context';

import Header from '../../components/Header';
import ProductTile from '../../components/ProductTile';

import { ScrollAppView } from "../../global/styles";
import heart from '../../../assets/heart/heartIcon.png'

import {
    ProductListContainer,
    FavoritesTitle,
    Row,
    Body,
    Column,
    FavoritesSubTitle,
    MyImage,
    ButtonText,
    Button,
  } from "./styles";

import { NavigationContainer } from '@react-navigation/native';

import { AuthContext } from '../../contexts/auth';
import { useNavigation } from '@react-navigation/native';

import ProductService from "../../services/ProductService";
import UserService from '../../services/UserService';



interface Product {
    id: number;
    name: string;
    pricetag: number;
    rating: number;
    Image: {};
    supplier: string;
  }


const Favorites = () => {

    const [products, setProducts] = useState<Product[]>([]);
    const [hasItens, setHasItens] = useState (false) 

    

    useEffect(() => {
    
        ProductService.listProducts().then(response => {
            console.log(response);
            if(response) setProducts(response.data.products);
            })
        }, []);
    

const Auth = useContext(AuthContext);
const navigation =  useNavigation();

function handleNavigateToProduct(id: number) {
    navigation.navigate('Product',{ id });
}


function handleNavigateToFavorites(id: number) {
    if (Auth.token == "") {
        noAccountAlert();
    } else {

    navigation.navigate('Favorites', { id });
}}
function noAccountAlert() {
    throw new Error('Function not implemented.');
}


    return ( 

        <View>
        <Header/>
    
        
        <Body>
        <FavoritesTitle>Favoritos</FavoritesTitle>
        {hasItens?
        <Row>
            <Column>
                <ProductListContainer>
                { products.map(event => {
                  return(
                

                <ProductTile key={product.id} data={product}>

                    onPress={()=>{handleNavigateToProduct(product.id)}}

                </ProductTile>)
                ;
                  })}
                
               
    
                </ProductListContainer>
            </Column>
           
            <Column>
                <ProductListContainer>
                    {/* <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/>
                    <ProductTile/> */}
                </ProductListContainer>
            </Column>
        </Row>
   
    :
       
    <View>
        <View style={{flex:1, backgroundColor:"#102024"}}>
        
    
        <Body>
        <FavoritesSubTitle> Sua lista está vazia</FavoritesSubTitle>
        <MyImage source={heart} resizeMode='contain'/>
        <Button onPress={()=> navigation.navigate("Home")} >
        <ButtonText>Adicione Itens</ButtonText>
        </Button>
        </Body>
       </View>
    </View>
       
    }

        <View style={{height: 200}}></View>
        </Body>
       </View>

    );

}





export default Favorites;







