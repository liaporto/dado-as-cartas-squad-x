import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useEffect, useState } from 'react'; 


interface Data {
    token: string;
    setToken: any;
    signed: boolean;
    userId: string;
    setUser: any;
}


export const AuthContext = createContext<Data>({} as Data);

const AuthProvider = (props:any) => {
    
    const [ authorization, setAuthorization ] = useState("");
    const [userId, setUserId] = useState("");
    const [ checkLogIn, setCheckLogIn ] = useState(false);

    const user_token = async () => {
        let token = '';
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                token = 'Bearer '.concat(value);
                return token;
            }
        }   catch(e) {
            console.log("Sem token.")
        }
        return token;
    }

    const user_id = async () => {
        let id = '';
        try{
            const value = await AsyncStorage.getItem('userId');
            if(value !== null){
               id = value;
            }
        } catch(err){
            console.log(err);
        }
        return id;
    }

    function checkIsLoggedIn() {
        if (authorization) {
            setCheckLogIn(true);
        }else {
            setCheckLogIn(false);
        }
    }


    useEffect(() => {
        user_token().then(value => {
            setAuthorization(value);
        });

        user_id().then(value => {
            setUserId(value);
        })
    }, [])
    

    useEffect(() => {
        checkIsLoggedIn();
    }, [authorization, checkLogIn])

    
    return(
        <AuthContext.Provider value={{token: authorization, setToken: 
        setAuthorization, signed: checkLogIn, userId: userId, setUser: setUserId}}>
            {props.children}
        </AuthContext.Provider>
    );
} 
 

export default AuthProvider;
